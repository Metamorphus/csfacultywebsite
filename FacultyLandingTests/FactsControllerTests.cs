﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;
using Moq;
using FacultyLanding.Models;
using Microsoft.AspNetCore.Mvc;
using FacultyLanding.Areas.Api.Controllers;

namespace FLTests
{
    public class FactsControllerTests
    {
        private Fact fact = new Fact()
        {
            FactId = 1,
            Pic = "url",
            Content = "text"
        };
        private Fact factFroPutTest = new Fact()
        {
            FactId = 2,
            Pic = "urlName",
            Content = "Content"
        };
        //Get START TESTS
        [Fact]
        public void GetFactById_ValidId_ReturnsFact()
        {
            var mockRepo = new Mock<IFacultyRepository>();
            mockRepo.Setup(repo => repo.GetFact(fact.FactId)).Returns(fact);
            var controller = new FactsController(mockRepo.Object);

            var result = controller.Get(fact.FactId);
            Assert.IsType(typeof(JsonResult), result);
            var jResult = result as JsonResult;

            Assert.Equal(fact, jResult.Value);
        }

        [Fact]
        public void GetFactById_InvalidId_ReturnsNotFound()
        {
            var mockRepo = new Mock<IFacultyRepository>();
            mockRepo.Setup(repo => repo.GetFact(fact.FactId)).Returns(fact);
            var controller = new FactsController(mockRepo.Object);

            var result = controller.Get(100);

            Assert.IsType(typeof(NotFoundResult), result);
        }
        //Get END TESTS

        //POST START TESTS
        [Fact]
        public void Valid_AddFact_ReturnsCreatedAtRoute()
        {
            var mockRepo = new Mock<IFacultyRepository>();
            var controller = new FactsController(mockRepo.Object);

            IActionResult result = controller.Post(fact);

            Assert.IsType(typeof(CreatedAtRouteResult), result);
            var routeResult = result as CreatedAtRouteResult;
            Assert.Equal(201, routeResult.StatusCode);
            Assert.Equal(fact.FactId, routeResult.RouteValues["id"]);
            Assert.Equal("GetFact", routeResult.RouteName);
        }

        [Fact]
        public void Invalid_AddFact_ReturnsBadRequest()
        {
            Fact wrondFact = null;
            var mockRepo = new Mock<IFacultyRepository>();
            var controller = new FactsController(mockRepo.Object);

            IActionResult result = controller.Post(wrondFact);

            Assert.IsType(typeof(BadRequestResult), result);
        }

        //POST END TESTS

        //PUT START TESTS
        [Fact]
        public void Valid_UpdateFact_ReturnsNoContent()
        {
            var mockRepo = new Mock<IFacultyRepository>();
            mockRepo.Setup(repo => repo.GetFact(fact.FactId)).Returns(fact);
            var controller = new FactsController(mockRepo.Object);

            var result = controller.Put(fact.FactId, factFroPutTest);

            Assert.IsType(typeof(NoContentResult), result);
            mockRepo.Verify(f => f.UpdateFact(factFroPutTest), Times.Once());
        }
        [Fact]
        public void Invalid_UpdateFact_ReturnsNoContent()
        {
            var mockRepo = new Mock<IFacultyRepository>();
            var controller = new FactsController(mockRepo.Object);

            var result = controller.Put(100, factFroPutTest);

            Assert.IsType(typeof(NotFoundResult), result);
        }

        //PUT END TESTS

        //DELETE START TESTS
        [Fact]
        public void ValidId_DeleteFact_ReturnsNoContentResult()
        {
            var mockRepo = new Mock<IFacultyRepository>();
            mockRepo.Setup(repo => repo.GetFact(fact.FactId)).Returns(fact);
            var controller = new FactsController(mockRepo.Object);

            var result = controller.Delete(fact.FactId);
            Assert.IsType(typeof(NoContentResult), result);
        }

        [Fact]
        public void InvalidId_DeleteFact_ReturnsNotFound()
        {
            var mockRepo = new Mock<IFacultyRepository>();
            var controller = new FactsController(mockRepo.Object);

            IActionResult result = controller.Delete(100);
            Assert.IsType(typeof(NotFoundResult), result);
        }
        //DELETE END TESTS
    }
}
