﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;
using Moq;
using FacultyLanding.Models;
using Microsoft.AspNetCore.Mvc;
using FacultyLanding.Areas.Api.Controllers;

namespace FLTests
{
    public class NewsItemsControllerTests
    {
        private NewsItem ValidNewsItem = new NewsItem()
        {
            NewsItemId = 1,
            Title = "FirstNI",
            Snippet = "Snippet",
            Content = "NIC",
            PhotoLink = "SomePhoto"
        };
        private NewsItem NewsItemForPut = new NewsItem()
        {
            NewsItemId = 2,
            Title = "FirstINI",
            Snippet = "INISnippet",
            Content = "INIC",
            PhotoLink = "INISomePhoto"
        };

        //POST START
        [Fact]
        public void Valid_AddNewsItem_ReturnsCreatedAtRoute()
        {
            var mockRepo = new Mock<IFacultyRepository>();
            var controller = new NewsItemsController(mockRepo.Object);

            IActionResult result = controller.Post(ValidNewsItem);

            Assert.IsType(typeof(CreatedAtRouteResult), result);
            var routeResult = result as CreatedAtRouteResult;
            Assert.Equal(201, routeResult.StatusCode);
            Assert.Equal(ValidNewsItem.NewsItemId, routeResult.RouteValues["id"]);
            Assert.Equal("GetNewsItem", routeResult.RouteName);
        }
        [Fact]
        public void Null_AddNewsItem_ReturnsBadRequest()
        {
            var mockRepo = new Mock<IFacultyRepository>();
            var controller = new NewsItemsController(mockRepo.Object);

            IActionResult result = controller.Post(null);

            Assert.IsType(typeof(BadRequestResult), result);
        }
        //POST END

        //GET START
        [Fact]
        public void GetNewsItemById_ValidId_ReturnsNewsItem()
        {
            var mockRepo = new Mock<IFacultyRepository>();
            mockRepo.Setup(repo => repo.GetNewsItem(ValidNewsItem.NewsItemId)).Returns(ValidNewsItem);
            var controller = new NewsItemsController(mockRepo.Object);

            var result = controller.Get(ValidNewsItem.NewsItemId);
            Assert.IsType(typeof(JsonResult), result);
            var jResult = result as JsonResult;

            Assert.Equal(ValidNewsItem, jResult.Value);
        }

        [Fact]
        public void GetFactById_InvalidId_ReturnsNotFound()
        {
            var mockRepo = new Mock<IFacultyRepository>();
            mockRepo.Setup(repo => repo.GetNewsItem(ValidNewsItem.NewsItemId)).Returns(ValidNewsItem);
            var controller = new FactsController(mockRepo.Object);

            var result = controller.Get(100);

            Assert.IsType(typeof(NotFoundResult), result);
        }
        //GET END

        //PUT START
        [Fact]
        public void Valid_UpdateNewsItem_ReturnsNoContent()
        {
            var mockRepo = new Mock<IFacultyRepository>();
            mockRepo.Setup(repo => repo.GetNewsItem(ValidNewsItem.NewsItemId)).Returns(ValidNewsItem);
            var controller = new NewsItemsController(mockRepo.Object);

            var result = controller.Put(ValidNewsItem.NewsItemId, NewsItemForPut);

            Assert.IsType(typeof(NoContentResult), result);
            mockRepo.Verify(f => f.UpdateNewsItem(NewsItemForPut), Times.Once());
        }
        [Fact]
        public void Invalid_UpdateNewsItem_ReturnsNotFound()
        {
            var mockRepo = new Mock<IFacultyRepository>();
            var controller = new NewsItemsController(mockRepo.Object);

            var result = controller.Put(100, NewsItemForPut);

            Assert.IsType(typeof(NotFoundResult), result);
        }
        [Fact]
        public void Invalid_UpdateNewsItem_ReturnsBadRequest()
        {
            var mockRepo = new Mock<IFacultyRepository>();
            var controller = new NewsItemsController(mockRepo.Object);

            var result = controller.Put(NewsItemForPut.NewsItemId, null);

            Assert.IsType(typeof(BadRequestResult), result);
        }
        //PUT END

        //DELETE START
        [Fact]
        public void ValidId_DeleteNewsItem_ReturnsNoContentResult()
        {
            var mockRepo = new Mock<IFacultyRepository>();
            mockRepo.Setup(repo => repo.GetNewsItem(ValidNewsItem.NewsItemId)).Returns(ValidNewsItem);
            var controller = new NewsItemsController(mockRepo.Object);

            var result = controller.Delete(ValidNewsItem.NewsItemId);
            Assert.IsType(typeof(NoContentResult), result);
        }
        [Fact]
        public void InvalidId_DeleteNewsItem_ReturnsNotFound()
        {
            var mockRepo = new Mock<IFacultyRepository>();
            var controller = new NewsItemsController(mockRepo.Object);

            IActionResult result = controller.Delete(100);
            Assert.IsType(typeof(NotFoundResult), result);
        }
        //DELETE END
    }
}
