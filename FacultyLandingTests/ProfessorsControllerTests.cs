﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;
using Moq;
using FacultyLanding.Models;
using FacultyLanding.Areas;
using Microsoft.AspNetCore.Mvc;
using FacultyLanding.Areas.Api.Controllers;

namespace FLTests
{
    public class ProfCon
    {
        public class Get
        {
            [Fact]
            public void NoParameters_ReturnsFullList()
            {
                //Arrange
                var fullList = new List<Professor>() {
                    sampleProf1,
                    sampleProf2
                };
                var mockRepo = new Mock<IFacultyRepository>();
                mockRepo.Setup(repo => repo.GetAllProfessors()).Returns(fullList);
                var controller = new ProfessorsController(mockRepo.Object);

                //Act
                var result = controller.Get();

                //Assert
                Assert.IsType(typeof(JsonResult), result);
                JsonResult jsonResult = result as JsonResult;
                Assert.IsType(typeof(List<Professor>), jsonResult.Value);
                List<Professor> profs = jsonResult.Value as List<Professor>;
                Assert.Equal(fullList.Count, profs.Count);
            }

            [Fact]
            public void ValidId_ReturnsNeededProf()
            {
                //Arrange
                var mockRepo = new Mock<IFacultyRepository>();
                mockRepo.Setup(repo => repo.GetProfessor(sampleProf1.ProfessorId)).Returns(sampleProf1);
                var controller = new ProfessorsController(mockRepo.Object);

                //Act
                var result = controller.Get(sampleProf1.ProfessorId);

                //Assert
                Assert.IsType(typeof(JsonResult), result);
                JsonResult jsonResult = result as JsonResult;
                Assert.IsType(typeof(Professor), jsonResult.Value);
                Professor prof = jsonResult.Value as Professor;
                Assert.Equal(sampleProf1, prof);
            }

            [Fact]
            public void InvalidId_ReturnsNotFound()
            {
                //Arrange
                var mockRepo = new Mock<IFacultyRepository>();
                mockRepo.Setup(repo => repo.GetProfessor(sampleProf1.ProfessorId)).Returns(sampleProf1);
                var controller = new ProfessorsController(mockRepo.Object);

                //Act
                var result = controller.Get(777);

                //Assert
                Assert.IsType(typeof(NotFoundResult), result);
            }
        }

        public class Post
        {
            [Fact]
            public void ValidProf_ReturnsCreatedAtRoute()
            {
                //Arrange
                var mockRepo = new Mock<IFacultyRepository>();
                var controller = new ProfessorsController(mockRepo.Object);

                //Act
                var result = controller.Post(sampleProf1);

                //Assert
                Assert.IsType(typeof(CreatedAtRouteResult), result);
                var routeResult = result as CreatedAtRouteResult;
                Assert.Equal(201, routeResult.StatusCode);
                Assert.Equal("GetProfessor", routeResult.RouteName);
                Assert.Contains("id", routeResult.RouteValues.Keys);
                Assert.Equal(sampleProf1.ProfessorId, routeResult.RouteValues["id"]);
                mockRepo.Verify(r => r.AddProfessor(sampleProf1), Times.Once());
            }

            [Fact]
            public void NullProf_ReturnsBadRequest()
            {
                //Arrange
                var mockRepo = new Mock<IFacultyRepository>();
                var controller = new ProfessorsController(mockRepo.Object);

                //Act
                var result = controller.Post(null);

                //Assert
                Assert.IsType(typeof(BadRequestResult), result);
            }

            [Fact]
            public void InvalidProf_ReturnsBadRequest()
            {
                //Arrange
                var mockRepo = new Mock<IFacultyRepository>();
                var controller = new ProfessorsController(mockRepo.Object);
                controller.ModelState.AddModelError("bla", "bla");

                //Act
                var result = controller.Post(sampleProf3);

                //Assert
                Assert.IsType(typeof(BadRequestResult), result);
            }
        }

        public class Put
        {
            [Fact]
            public void ValidIdValidProf_ReturnsNoContent()
            {
                //Arrange
                var mockRepo = new Mock<IFacultyRepository>();
                mockRepo.Setup(r => r.GetProfessor(sampleProf1.ProfessorId))
                    .Returns(sampleProf1);
                var controller = new ProfessorsController(mockRepo.Object);

                //Act
                var result = controller.Put(sampleProf1.ProfessorId, sampleProf2);

                //Assert
                Assert.IsType(typeof(NoContentResult), result);
                mockRepo.Verify(r => r.GetProfessor(sampleProf1.ProfessorId), Times.Once());
                mockRepo.Verify(r => r.UpdateProfessor(sampleProf2), Times.Once());
            }

            [Fact]
            public void NullProf_ReturnsBadRequest()
            {
                //Arrange
                var mockRepo = new Mock<IFacultyRepository>();
                mockRepo.Setup(r => r.GetProfessor(sampleProf1.ProfessorId))
                    .Returns(sampleProf1);
                var controller = new ProfessorsController(mockRepo.Object);

                //Act
                var result = controller.Put(sampleProf1.ProfessorId, null);

                //Assert
                Assert.IsType(typeof(BadRequestResult), result);
            }

            [Fact]
            public void InvalidProf_ReturnsBadRequest()
            {
                //Arrange
                var mockRepo = new Mock<IFacultyRepository>();
                mockRepo.Setup(r => r.GetProfessor(sampleProf1.ProfessorId))
                    .Returns(sampleProf1);
                var controller = new ProfessorsController(mockRepo.Object);
                controller.ModelState.AddModelError("bla", "bla");

                //Act
                var result = controller.Put(sampleProf1.ProfessorId, sampleProf2);

                //Assert
                Assert.IsType(typeof(BadRequestResult), result);
            }

            [Fact]
            public void InvalidId_ReturnsNotFound()
            {
                //Arrange
                var mockRepo = new Mock<IFacultyRepository>();
                var controller = new ProfessorsController(mockRepo.Object);

                //Act
                var result = controller.Put(777, sampleProf2);

                //Assert
                Assert.IsType(typeof(NotFoundResult), result);
                mockRepo.Verify(r => r.GetProfessor(777), Times.Once());
            }
        }

        public class Delete
        {
            [Fact]
            public void ValidProf_ReturnsNoContent()
            {
                //Arrange
                var mockRepo = new Mock<IFacultyRepository>();
                mockRepo.Setup(r => r.GetProfessor(sampleProf1.ProfessorId))
                    .Returns(sampleProf1);
                var controller = new ProfessorsController(mockRepo.Object);

                //Act
                var result = controller.Delete(sampleProf1.ProfessorId);

                //Assert
                Assert.IsType(typeof(NoContentResult), result);
                mockRepo.Verify(r => r.GetProfessor(sampleProf1.ProfessorId), Times.Once());
                mockRepo.Verify(r => r.RemoveProfessor(sampleProf1.ProfessorId), Times.Once());
            }

            [Fact]
            public void InvalidId_ReturnsNotFound()
            {
                //Arrange
                var mockRepo = new Mock<IFacultyRepository>();
                var controller = new ProfessorsController(mockRepo.Object);

                //Act
                var result = controller.Delete(777);

                //Assert
                Assert.IsType(typeof(NotFoundResult), result);
                mockRepo.Verify(r => r.GetProfessor(777), Times.Once());
            }
        }

        private static readonly Professor sampleProf1 = new Professor()
        {
            ProfessorId = 1,
            Title = "lecturer",
            Name = "Afonin",
            PhotoLink = "/afonin",
            Portfolio = "cheerful"
        };

        private static readonly Professor sampleProf2 = new Professor()
        {
            ProfessorId = 2,
            Title = "lecturer",
            Name = "A. Glybovets",
            PhotoLink = "/aglyb",
            Portfolio = "best"
        };

        private static readonly Professor sampleProf3 = new Professor()
        {
            ProfessorId = 3,
            Title = "professor",
            Name = "Procenko",
            PhotoLink = "/procenko",
            Portfolio = "boring"
        };
    }
}
