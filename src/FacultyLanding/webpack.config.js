var path = require('path');
var webpack = require('webpack');
var merge = require('extendify')({ isDeep: true, arrays: 'concat' });

var isDevelopment = process.env.ASPNETCORE_ENVIRONMENT === 'Development';

var commonConfig = {
  entry: {
    'polyfills': ['./Frontend/polyfills.ts'],
    'vendor': ['./Frontend/vendor.ts'],
    'main': ['./Frontend/main.browser.ts']
  },

  resolve: {
    extensions: [ '', '.js', '.ts' ]
  },

  module: {
    loaders: [
      { test: /\.ts$/, loaders: ['awesome-typescript-loader', 'angular2-template-loader'] },
      { test: /\.scss$/, loaders: ['raw-loader', 'sass-loader'] },
      { test: /\.css$/, loader: 'raw-loader' },
      { test: /\.html$/, loader: 'raw-loader' }
    ]
  },

  output: {
    path: path.join(__dirname, 'wwwroot', 'dist'),
    filename: '[name].js',
    publicPath: '/dist/'
  },

  plugins: [
    new webpack.optimize.CommonsChunkPlugin({
      name: ['main', 'vendor', 'polyfills']
    }),
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify(isDevelopment ? 'development' : 'production')
    }),
  ],

  stats: { children: false }
};

var devConfig = {
  devtool: 'inline-source-map'
};

var prodConfig = {
  plugins: [
    new webpack.optimize.OccurenceOrderPlugin(),
    new webpack.optimize.UglifyJsPlugin({
      compress: { warnings: false },
      minimize: true,
      mangle: false // Due to https://github.com/angular/angular/issues/6678
    })
  ]
};

module.exports = merge(commonConfig, isDevelopment ? devConfig : prodConfig);
