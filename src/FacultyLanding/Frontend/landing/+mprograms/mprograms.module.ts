import { RouterModule, RouterConfig } from '@angular/router';
import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';

import { MprogramsComponent } from './mprograms.component';

export const ROUTER_CONFIG = [
  { path: 'mprograms/:id', component: MprogramsComponent, pathMatch: 'full' }
];

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(ROUTER_CONFIG)
  ],
  declarations: [
    MprogramsComponent
  ]
})
export class MProgramsModule {
}
