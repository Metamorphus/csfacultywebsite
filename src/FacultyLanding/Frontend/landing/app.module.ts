import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { routing } from './app.routing';

import { ErrorModule } from './+error/error.module';
import { HomeModule } from './+home/home.module';
import { BlogModule } from './+blog/blog.module';
import { BprogramsModule } from './+bprograms/bprograms.module';
import { ProfessorsModule } from './+professors/professors.module';
import { MProgramsModule } from "./+mprograms/mprograms.module";

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    routing,
    ErrorModule,
    HomeModule,
    BlogModule,
    BprogramsModule,
    ProfessorsModule,
    MProgramsModule
  ],
  declarations: [ AppComponent ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
