import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Title } from '@angular/platform-browser';

import { Professor} from '../shared/models/professor';
import { ApiService } from '../shared/api.service';
import { ENDPOINTS } from '../shared/api';
import { pageTitle } from '../shared/page-title';

@Component({
    selector: 'professor',
    templateUrl: 'professor.component.html',
    styleUrls: ['professor.component.scss']
})
export class ProfessorComponent implements OnInit {
  professor: Professor = null;
  isFetching = true;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private titleService: Title,
              public api: ApiService) {
    titleService.setTitle(pageTitle('Викладачі'));
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      let id = +params['id'];
      this.api.getById(ENDPOINTS['professors'], id)
        .subscribe(
          res=>{
            this.professor = res;
            this.isFetching=false;
          },
          error => {
            if (error.status == 404)
              this.router.navigate(['/error/notfound']);
          }
        );
    });
  }
}
