import { RouterModule, RouterConfig } from '@angular/router';
import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';

import { ProfessorsComponent } from './professors.component';
import {ProfessorComponent} from "./professor.component";

export const ROUTER_CONFIG = [
  { path: 'professors', component: ProfessorsComponent, pathMatch: 'full' },
  { path: 'professors/:id', component: ProfessorComponent, pathMatch: 'full' }
];

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(ROUTER_CONFIG)
  ],
  declarations: [
    ProfessorsComponent,
    ProfessorComponent
  ]
})
export class ProfessorsModule {
}
