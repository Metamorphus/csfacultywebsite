import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Title } from '@angular/platform-browser';

import { Professor} from '../shared/models/professor';
import { ApiService } from '../shared/api.service';
import { ENDPOINTS } from '../shared/api';
import { pageTitle } from '../shared/page-title';

@Component({
    selector: 'professors',
    templateUrl: 'professors.component.html',
    styleUrls: ['professors.component.scss']
})
export class ProfessorsComponent implements OnInit {
  professors: Professor[]=null;
  isFetching=true;

  groupBy(xs,key) {
    return xs.reduce(function(rv, x) {
      (rv[x[key]] = rv[x[key]] || []).push(x);
      return rv;
    }, {});
  };

  constructor(private route: ActivatedRoute,
              private router: Router,
              private titleService: Title,
              public api: ApiService) {
    titleService.setTitle(pageTitle('Викладачі'));
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.api.getAll(ENDPOINTS['professors'])
        .subscribe(
          res=>{
            this.professors = res;
            this.isFetching=false;
          }
        );
    });
  }
}
