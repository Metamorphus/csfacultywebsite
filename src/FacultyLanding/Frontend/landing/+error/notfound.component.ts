import { Component, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Title } from '@angular/platform-browser';

import { pageTitle } from '../shared/page-title';

@Component({
  selector: 'notfound',
  templateUrl: 'notfound.component.html',
  styleUrls: ['notfound.component.scss']
})
export class NotfoundComponent {
  constructor(private titleService: Title) {
    titleService.setTitle(pageTitle('Сторінку не знайдено'));
  }
}
