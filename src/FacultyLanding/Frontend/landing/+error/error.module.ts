import { RouterModule, RouterConfig } from '@angular/router';
import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';

import { NotfoundComponent } from './notfound.component';

export const ROUTER_CONFIG = [
  { path: 'error/notfound', component: NotfoundComponent, pathMatch: 'full' }
];

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(ROUTER_CONFIG)
  ],
  declarations: [
    NotfoundComponent
  ]
})
export class ErrorModule {
}
