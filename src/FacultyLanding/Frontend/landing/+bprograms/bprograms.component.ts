import { Component, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Title } from '@angular/platform-browser';

import { UProgram } from '../shared/models/uprogram';
import { ApiService } from '../shared/api.service';
import { ENDPOINTS } from '../shared/api';
import { pageTitle } from '../shared/page-title';

@Component({
  selector: 'bprograms',
  styleUrls: ['bprograms.component.scss'],
  templateUrl: 'bprograms.component.html'
})
export class BprogramsComponent {
  isFetching = true;
  program: UProgram = null;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private titleService: Title,
              public api: ApiService) {
    titleService.setTitle(pageTitle('Бакалаврат'));
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      let id = +params['id'];
      this.api.getById(ENDPOINTS['uprograms'], id)
        .subscribe(
          res=>{
            this.program = res;
            this.isFetching=false;
          },
          error => {
            if (error.status == 404)
              this.router.navigate(['/error/notfound']);
          }
        );
    });
  }
}
