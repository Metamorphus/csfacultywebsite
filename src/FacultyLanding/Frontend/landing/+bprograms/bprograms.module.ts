import { RouterModule, RouterConfig } from '@angular/router';
import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';

import { BprogramsComponent } from './bprograms.component';

export const ROUTER_CONFIG = [
  { path: 'bprograms/:id', component: BprogramsComponent, pathMatch: 'full' }
];

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(ROUTER_CONFIG)
  ],
  declarations: [
    BprogramsComponent
  ]
})
export class BprogramsModule {
}
