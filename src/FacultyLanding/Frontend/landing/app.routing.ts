import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const appRoutes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'error', loadChildren: './+error/error.module' },
  { path: 'home', loadChildren: './+home/home.module' },
  { path: 'blog', loadChildren: './+blog/blog.module' },
  { path: 'bprograms', loadChildren: './+bprograms/bprograms.module' },
  { path: 'mprograms', loadChildren: './+mprograms/mprograms.module' },
  { path: 'professors', loadChildren: './+professors/professors.module' },
  { path: '**', redirectTo: 'error/notfound' },
];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);
