
export const HOST = "http://localhost:5000";

export const ENDPOINTS = {
  uprograms: HOST + "/api/undergradprograms",
  mprograms: HOST + "/api/masterprograms",
  news: HOST + "/api/newsitems",
  facts: HOST + "/api/facts",
  professors: HOST + "/api/professors",
  companies: HOST + "/api/companies",
  reviews: HOST + "/api/reviews",
  contacts: HOST + "/api/contacts",
  message:HOST+"/api/mail"
}
