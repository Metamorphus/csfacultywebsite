const COMMON_TITLE = "Факультет Інформатики НаУКМА";

export function pageTitle(s?: string) {
  return s ? s + " - " + COMMON_TITLE : COMMON_TITLE;
}
