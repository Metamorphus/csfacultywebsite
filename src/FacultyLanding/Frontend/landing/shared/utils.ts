declare let $:any;

function loop(elements, i, timeout) {
  setTimeout((l, index)=> {
    l.eq(index).show();
    if (i < l.length)
      loop(l, index + 1,timeout);
  }, timeout, elements, i);
}
