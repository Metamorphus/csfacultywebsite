import {Component, OnInit, AfterViewInit, Input} from '@angular/core';
import { PageScrollConfig } from 'ng2-page-scroll';

declare var $:any;
@Component({
    selector: 'ng-header',
    templateUrl: 'header.component.html',
    styleUrls: ['header.component.scss']
})
export class HeaderComponent  implements  OnInit{

  @Input() isFixed=false;
 get  opc_header(){

   return (window.scrollY>300?false:true)&&this.isFixed;
 };

  ngOnInit(){
  }



  constructor() {
    PageScrollConfig.defaultDuration = 300;
    PageScrollConfig.defaultScrollOffset = 80;
  }
}
