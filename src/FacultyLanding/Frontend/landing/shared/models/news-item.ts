export class NewsItem {
  public newsItemId: number;
  public title: string;
  public snippet: string;
  public content: string;
  public publicationDate: Date;
  public photoLink: string;
}
