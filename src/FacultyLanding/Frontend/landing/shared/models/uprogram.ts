import { Professor } from './professor';

export class UProgram {
  constructor(UpId: number,
              public Code: string,
              public Name: string,
              public Description: string,
              public Professor: Professor) {
  }
}
