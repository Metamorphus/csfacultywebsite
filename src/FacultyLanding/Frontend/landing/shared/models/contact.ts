export class Contact {
  public country: string;
  public city: string;
  public zipCode: string;
  public street: string;
  public building: number;
  public office: number;
  public telephone1: string;
  public telephone2: string;
  public fax: string;
  public email: string;
  public website: string;
  public latitude: number;
  public longitude: number;
}
