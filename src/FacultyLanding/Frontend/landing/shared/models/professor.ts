export class Professor {
  constructor(
    public PId:number,
    public Name:string,
    public Title:string,
    public Position:string,
    public Portfolio:string,
    public PhotoLink:string,
    public Rank:number,
    public Phone:string,
    public Email:string,
    public Website:string
  ) {}
}
