import {Component, OnInit, AfterViewInit} from '@angular/core';

@Component({
  selector: 'ng-footer',
  templateUrl: 'footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit, AfterViewInit {
  get visible(){
    return window.scrollY>1000?'block':'none';
  }
  
  constructor() { }

  ngAfterViewInit() { }

  ngOnInit() { }
}
