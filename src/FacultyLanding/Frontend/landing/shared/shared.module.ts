import { NgModule } from '@angular/core';
import {BrowserModule, Title} from '@angular/platform-browser'
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { PageScroll } from 'ng2-page-scroll';
import {
  GOOGLE_MAPS_DIRECTIVES,
  GOOGLE_MAPS_PROVIDERS
} from 'angular2-google-maps/core';

import { ApiService } from './api.service';
import { HeaderComponent } from './header.component';
import { FooterComponent } from './footer.component';
import { LoaderComponent } from './loader.component';
import {TabsModule} from "ng2-tabs";

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    TabsModule,
    FormsModule
  ],
  providers: [
    GOOGLE_MAPS_PROVIDERS,
    ApiService,
    Title
  ],
  declarations: [
    PageScroll,
    GOOGLE_MAPS_DIRECTIVES,
    HeaderComponent,
    FooterComponent,
    LoaderComponent
  ],
  exports: [
    TabsModule,
    PageScroll,
    GOOGLE_MAPS_DIRECTIVES,
    HeaderComponent,
    FooterComponent,
    LoaderComponent,
    CommonModule,
    FormsModule,
    BrowserModule
  ]
})
export class SharedModule { }
