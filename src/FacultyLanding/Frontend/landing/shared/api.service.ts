import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs';

@Injectable()
export class ApiService {
    constructor(public http:Http) { }

    private get(url: string): Observable<any> {
      return this.http.get(url)
                 .retryWhen(errors => {
                   return errors.flatMap(err => {
                     if (process.env.NODE_ENV === 'development')
                       console.log(err);
                     if (err.status == 404)
                       return Observable.throw(err);
                     else
                       return Observable.timer(1000);
                    });
                 })
                 .map(data => {
                   if (process.env.NODE_ENV === 'development')
                    console.log(data.json());
                   return data.json()
                 });
    }

    public getAll(url: string): Observable<any> {
      return this.get(url);
    }

    public getById(url: string, id: number) {
      return this.get(url+'/'+id);
    }

    public post(url,obj){
      return this.http.post(url,obj);
    }
}
