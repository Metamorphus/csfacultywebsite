import { Component, OnInit ,AfterViewInit} from '@angular/core';
import {Router, ActivatedRoute} from "@angular/router";
import {ApiService} from "../shared/api.service";
import {UProgram} from "../shared/models/uprogram";
import {ENDPOINTS} from "../shared/api";
declare  var $:any;
@Component({
  selector: 'mprograms',
  templateUrl: 'mprograms.html',
  styleUrls:['./mprograms.scss']
})
export class MProgramsComponent implements OnInit,AfterViewInit {
  constructor(private route: ActivatedRoute,
              private router: Router, public api: ApiService) {
  }

  Mprograms;
  Professor;
  isFetching=true;
  tabActive='1';
  isActive(e){
    return this.tabActive===e;

  }

  ngAfterViewInit(){
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.api.getAll(ENDPOINTS['mprograms'])
        .subscribe(
          res=>{
            for(let i=0;i<res.length;i++){
              console.log(res);
              this.api.getById(ENDPOINTS['professors'],res[i].professorId)
                .subscribe(r=>{console.log(r);res[i].professor=r},err=>console.log(err));
            }

            this.Mprograms = res;



            this.isFetching=false;

          },
          err=> {
            //  this.router.navigate(['/error', err]);
            this.isFetching=true;
          }
        );
    });
  }

}
