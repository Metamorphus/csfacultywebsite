import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {ApiService} from "../shared/api.service";
import {ENDPOINTS} from "../shared/api";

@Component({
  selector: 'aboutus',
  templateUrl: 'aboutus.html',
  styleUrls:['./aboutus.scss']
})
export class AboutUsComponent implements OnInit {

  constructor(private route: ActivatedRoute,
              private router: Router, public api: ApiService) {
  }

  Reviews;
  isFetching=true;

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.api.getAll(ENDPOINTS['reviews'])
        .subscribe(
          res=>{
            this.Reviews = res;
            this.isFetching=false;

          },
          err=> {
            //  this.router.navigate(['/error', err]);
            this.isFetching=true;
          }
        );
    });
  }

}
