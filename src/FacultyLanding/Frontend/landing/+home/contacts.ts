import {
  Component,
  OnInit,
  AfterViewInit
} from '@angular/core';

import {
  ApiService
} from '../shared/api.service';
import {
  ENDPOINTS
} from '../shared/api';
import {
  Contact
} from '../shared/models/contact';

@Component({
  selector: 'contacts',
  templateUrl: 'contacts.html',
  styleUrls: ['./contacts.scss']
})
export class ContactsComponent implements OnInit {
  isFetching: boolean = true;
  isFormActive = true;
  formError = null;
  isFormSending=false;
  contact: Contact;
  submited=false;


  name: string;
  email: string;
  message: string;

  constructor(
    private api: ApiService
  ) {}

  ngOnInit() {
    this.api.getAll(ENDPOINTS['contacts']).toPromise()
      .then(data => {
        this.contact = data[0];
        this.isFetching = false;
      });
  }

  sendMessage() {
    console.log(this.name, this.email, this.message);
    

    this.submited=true;
 
    this.api.post(ENDPOINTS['message'], {
        name: this.name,
        email: this.email,
        message: this.message
     
      })
      .subscribe(res => {
         this.isFormActive = false; 
    
        console.log(res);
      }, err => {
        console.log(err);
        this.formError = "помилка відправлення"
        this.isFormActive=true;
           this.submited=false;
     
      })
     


  }
}