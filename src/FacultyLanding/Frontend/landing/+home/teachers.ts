import {Component, OnInit, AfterViewInit} from '@angular/core';
import {Professor} from "../shared/models/professor";
import {ActivatedRoute, Router} from "@angular/router";
import {ApiService} from "../shared/api.service";
import {ENDPOINTS} from "../shared/api";

@Component({
  selector: 'teachers',
  templateUrl: 'teachers.html',
  styleUrls:['./teachers.scss']
})
export class TeachersComponent implements OnInit,AfterViewInit {
  constructor(private route: ActivatedRoute,
              private router: Router, public api: ApiService) {
  }

  Professors:Professor[];
  isFetching=true;



  ngAfterViewInit(){
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.api.getAll(ENDPOINTS['professors'])
        .subscribe(
          res=>{
            this.Professors = res;
            this.isFetching=false;

          },
          err=> {
            //  this.router.navigate(['/error', err]);
            this.isFetching=true;
          }
        );
    });
  }

}
