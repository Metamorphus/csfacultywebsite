import {Component, OnInit, AfterViewInit} from '@angular/core';
import {ENDPOINTS} from "../shared/api";
import {ApiService} from "../shared/api.service";
import {Router, ActivatedRoute} from "@angular/router";
declare  var $:any;

@Component({
  selector: 'facts',
  templateUrl: 'facts.html',
  styleUrls:['./facts.scss']
})
export class FactsComponent implements OnInit ,AfterViewInit {


  ngAfterViewInit(){
    $(window).on('scroll',function (e) {
      var scroll_poss=$(window).scrollTop();
      if(scroll_poss<1000){
        $('.scroll-top').hide();
      }else {
        $('.scroll-top').show();
      }

    });

  }
  constructor(private route: ActivatedRoute,
              private router: Router, public api: ApiService) {
  }

  Facts;
  isFetching=true;

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.api.getAll(ENDPOINTS['facts'])
        .subscribe(
          res=>{
            this.Facts = res;
            this.isFetching=false;

          },
          err=> {
            //  this.router.navigate(['/error', err]);
            this.isFetching=true;
          }
        );
    });
  }


}
