import { Component, OnInit ,AfterViewInit} from '@angular/core';
import {ApiService} from "../shared/api.service";
import {ActivatedRoute, Router} from "@angular/router";
import {ENDPOINTS} from "../shared/api";
import {UProgram} from "../shared/models/uprogram";

declare  var $:any;
@Component({
    selector: 'bprograms',
    templateUrl: 'bprograms.html',
    styleUrls:['./bprograms.scss']
})
export class BprogramsComponent implements OnInit,AfterViewInit {
  constructor(private route: ActivatedRoute,
              private router: Router, public api: ApiService) {
  }

  Uprograms:UProgram[];
  isFetching=true;
  tabActive='1';

  ngAfterViewInit(){
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.api.getAll(ENDPOINTS['uprograms'])
        .subscribe(
          res=>{

            for(let i=0;i<res.length;i++){
              console.log(res);
              this.api.getById(ENDPOINTS['professors'],res[i].professorId)
                .subscribe(r=>{console.log(r);res[i].professor=r},err=>console.log(err));
            }
            this.Uprograms = res;
            this.isFetching=false;

          },
          err=> {
            //  this.router.navigate(['/error', err]);
            this.isFetching=true;
          }
        );
    });
  }

}
