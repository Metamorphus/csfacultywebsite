import { RouterModule, RouterConfig } from '@angular/router';
import { NgModule } from '@angular/core';

import { SharedModule } from '../shared/shared.module';
import { HomeComponent } from './home.component';
import { BprogramsComponent } from './bprograms';
import { CarouselComponent } from './carousel';
import { TeachersComponent } from './teachers';
import { MProgramsComponent } from './mprograms';
import { FactsComponent } from './facts';
import { StudentWorkComponent } from './companies';
import { NewsComponent } from './news';
import { ContactsComponent } from './contacts';
import { AboutUsComponent } from './aboutus';

export const ROUTER_CONFIG = [
  { path: 'home', component: HomeComponent, pathMatch: 'full' }
];

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(ROUTER_CONFIG)
  ],
  declarations: [
    HomeComponent,
    BprogramsComponent,
    CarouselComponent,
    TeachersComponent,
    MProgramsComponent,
    FactsComponent,
    StudentWorkComponent,
    NewsComponent,
    ContactsComponent,
    AboutUsComponent,
  ]
})
export class HomeModule {
}
