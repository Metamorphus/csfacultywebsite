import { Component, OnInit } from '@angular/core';
import { ENDPOINTS } from '../shared/api';
import { ApiService } from '../shared/api.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'companies',
  templateUrl: 'companies.html',
  styleUrls: ['./companies.scss']
})
export class StudentWorkComponent implements OnInit {
  constructor(private route: ActivatedRoute,
              private router: Router,
              public api: ApiService) { }

  Companies;
  isFetching = true;

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.api.getAll(ENDPOINTS['companies'])
        .subscribe(
          res => {
            this.Companies = res;
            this.isFetching=false;
        
          }
        );
    });
  }
}
