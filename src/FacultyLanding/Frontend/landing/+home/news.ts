import { Component, OnInit } from '@angular/core';

import { ApiService } from '../shared/api.service';
import { ENDPOINTS } from '../shared/api';
import { NewsItem } from '../shared/models/news-item';

@Component({
  selector: 'news',
  templateUrl: 'news.html',
  styleUrls: ['./news.scss']
})
export class NewsComponent implements OnInit {
  isFetching: boolean = true;
  items: NewsItem[];

  constructor(
    private api: ApiService
  ) { }

  ngOnInit() {
    this.api.getAll(ENDPOINTS['news']).toPromise()
        .then(data => {
          this.items = data;
          this.items.sort((a, b) => {
            let x = a.publicationDate;
            let y = b.publicationDate;
            return ((x < y) ? 1 : ((x > y) ? -1 : 0));
          });
          this.isFetching = false;
        });
  }
}
