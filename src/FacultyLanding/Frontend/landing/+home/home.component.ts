import { Component } from '@angular/core';
import { pageTitle } from '../shared/page-title';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'home',
  template: `
    <ng-header [isFixed]="true"></ng-header>
    <main>
      <carousel></carousel>
      <bprograms></bprograms>
      <mprograms></mprograms>
      <teachers></teachers>
      <facts></facts>
      <news></news>
      <companies></companies>
      <aboutus></aboutus>
      <contacts></contacts>
    </main>
    <ng-footer></ng-footer>
  `
})
export class HomeComponent {
  constructor(private titleService: Title) {
    titleService.setTitle(pageTitle());
  }
}
