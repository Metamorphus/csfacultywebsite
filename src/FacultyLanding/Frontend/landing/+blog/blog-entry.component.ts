import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Title } from '@angular/platform-browser';

import { ApiService } from '../shared/api.service';
import { ENDPOINTS } from '../shared/api';
import { NewsItem } from '../shared/models/news-item';
import { pageTitle } from '../shared/page-title';

@Component({
  selector: 'blog-entry',
  templateUrl: 'blog-entry.component.html',
  styleUrls: ['blog-entry.component.scss']
})
export class BlogEntryComponent implements OnInit {
  isFetching: boolean = true;
  item: NewsItem;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private titleService: Title,
              public api: ApiService) {
    titleService.setTitle(pageTitle())
  }


  ngOnInit() {
    this.route.params.subscribe(params => {
      let id = +params['id'];
      this.api.getById(ENDPOINTS['news'], id).toPromise()
          .then(data => {
            this.item = data;
            this.isFetching = false;
            this.titleService.setTitle(pageTitle(data.title));
          })
          .catch(error => {
            if (error.status == 404)
              this.router.navigate(['/error/notfound']);
          })
    })
  }
}
