import { RouterModule, RouterConfig } from '@angular/router';
import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';

import { BlogComponent } from './blog.component';
import { BlogEntryComponent } from './blog-entry.component';

export const ROUTER_CONFIG = [
  { path: 'blog', component: BlogComponent },
  { path: 'blog/:id', component: BlogEntryComponent }
];

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(ROUTER_CONFIG)
  ],
  declarations: [
    BlogComponent,
    BlogEntryComponent
  ]
})
export class BlogModule {
}
