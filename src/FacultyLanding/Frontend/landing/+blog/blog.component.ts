import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Title } from '@angular/platform-browser';

import { ApiService } from '../shared/api.service';
import { ENDPOINTS } from '../shared/api';
import { NewsItem } from '../shared/models/news-item';
import { pageTitle } from '../shared/page-title';

@Component({
  selector: 'blog',
  templateUrl: 'blog.component.html',
  styleUrls: ['blog.component.scss']
})
export class BlogComponent implements OnInit {
  isFetching: boolean = true;
  items: NewsItem[];

  constructor(private route: ActivatedRoute,
              private router: Router,
              private titleService: Title,
              public api: ApiService) {
    titleService.setTitle(pageTitle('Новини'));
  }

  ngOnInit() {
    this.api.getAll(ENDPOINTS['news']).toPromise()
        .then(data => {
          this.items = data;
          this.items.sort((a, b) => {
            let x = a.publicationDate;
            let y = b.publicationDate;
            return ((x < y) ? 1 : ((x > y) ? -1 : 0));
          });
          this.isFetching = false;
        });
  }
}
