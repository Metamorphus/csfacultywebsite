﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Net.Http;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using FacultyLanding.Models;
using System.IO;
using Microsoft.AspNetCore.Http;
using System.Net.Http.Headers;
using Microsoft.EntityFrameworkCore;
using System.Text;
using FacultyLanding.Services;
using Microsoft.AspNetCore.Authorization;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace FacultyLanding.Areas.Adm.Controllers
{
    [Area("Admin")]
    public class ProfessorController : Controller
    {
        private readonly string _url = "http://localhost:58585/api/professors";
        private HttpClient _client;
        private IHostingEnvironment _environment;
        private readonly ILogger<ProfessorController> _logger;
        private readonly IEmailSender _emailSender;

        public ProfessorController(IHostingEnvironment environment,
            ILogger<ProfessorController> logger,
            IEmailSender sender)
        {
            _client = new HttpClient();
            _environment = environment;
            _logger = logger;
            _emailSender = sender;
        }

        // GET: NEWS
        public async Task<IActionResult> Index()
        {
            var log = HttpContext.Session.GetString("Log");

            if (log != "Admin")
                return RedirectToAction("Login", "Home");

            var task = await _client.GetAsync(_url);
            var stringResponse = await task.Content.ReadAsStringAsync();
            var professors = JsonConvert.DeserializeObject<IEnumerable<Professor>>(stringResponse).ToList();

            return View(professors);
        }

        public IActionResult Create()
        {
            var log = HttpContext.Session.GetString("Log");

            if (log != "Admin")
                return RedirectToAction("Login", "Home");

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ProfessorId,Title,Name,Position,Portfolio,PhotoLink,Phone,Email,Website")] Professor Professor, IFormFile file)
        {
            var log = HttpContext.Session.GetString("Log");

            if (log != "Admin")
                return RedirectToAction("Login", "Home");

            if (!Directory.Exists(_environment.WebRootPath + "/" + "upload/professors/"))
                Directory.CreateDirectory(_environment.WebRootPath + "/" + "upload/professors/");

            if (file == null)
            {
                Professor.PhotoLink = "https://storage.googleapis.com/prestashop/img/no_image.png";
            }
            else
            {
                List<string> validExtensions = new List<string> { ".jpg", ".png", ".jpeg" };
                var extension = Path.GetExtension(file.FileName);
                if (!validExtensions.Contains(extension))
                {
                    ModelState.AddModelError(String.Empty, "Valid extensions are .jpg, .png, .jpeg");
                    return View(Professor);
                }
                Professor.PhotoLink = String.Concat("upload/professors/", file.FileName);

                using (var fileStream = new FileStream(
                Path.Combine(_environment.WebRootPath, Professor.PhotoLink), FileMode.Create))
                {
                    await file.CopyToAsync(fileStream);
                }
            }

            Professor.Rank = 0;
            ModelState.Clear();
            this.TryValidateModel(Professor);

            if (ModelState.IsValid)
            {
                HttpContent content = new StringContent(JsonConvert.SerializeObject(Professor), Encoding.UTF8, "application/json");
                var post = await _client.PostAsync("http://localhost:58585/api/Professors", content);

                return RedirectToAction("Index");
            }
            return View(Professor);
        }

        // GET: Professor/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            var log = HttpContext.Session.GetString("Log");

            if (log != "Admin")
                return RedirectToAction("Login", "Home");

            if (id == null)
            {
                return NotFound();
            }

            var task = await _client.GetAsync(_url + $"/{id}");
            var streamPage = await task.Content.ReadAsStringAsync();

            var professor = JsonConvert.DeserializeObject<Professor>(streamPage);

            if (professor == null)
            {
                return NotFound();
            }

            return View(professor);
        }

        // GET: Professor/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            var log = HttpContext.Session.GetString("Log");

            if (log != "Admin")
                return RedirectToAction("Login", "Home");

            if (id == null)
            {
                return NotFound();
            }
            var task = await _client.GetAsync(_url + $"/{id}");
            var streamPage = await task.Content.ReadAsStringAsync();

            var Professor = JsonConvert.DeserializeObject<Professor>(streamPage);

            if (Professor == null)
            {
                return NotFound();
            }
            return View(Professor);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ProfessorId,Title,Name,Position,Portfolio,PhotoLink,Phone,Email,Website")] Professor Professor, IFormFile file)
        {
            var log = HttpContext.Session.GetString("Log");

            if (log != "Admin")
                return RedirectToAction("Login", "Home");

            if (id != Professor.ProfessorId)
            {
                return NotFound();
            }

            if (file != null)
            {
                List<string> validExtensions = new List<string> { ".jpg", ".png", ".jpeg" };
                var extension = Path.GetExtension(file.FileName);
                if (!validExtensions.Contains(extension))
                {
                    ModelState.AddModelError(String.Empty, "Valid extensions are .jpg, .png, .jpeg");
                    return View(Professor);
                }
                Professor.PhotoLink = String.Concat("upload/professors/", file.FileName);

                using (var fileStream = new FileStream(
                Path.Combine(_environment.WebRootPath, Professor.PhotoLink), FileMode.Create))
                {
                    await file.CopyToAsync(fileStream);
                }
            }

            ModelState.Clear();
            this.TryValidateModel(Professor);

            if (ModelState.IsValid)
            {
                try
                {
                    _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    HttpContent content = new StringContent(JsonConvert.SerializeObject(Professor), Encoding.UTF8, "application/json");
                    var put = await _client.PutAsync(_url + $"/{id}", content);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (Professor.ProfessorId == 0)
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            return View(Professor);
        }

        // GET: Professor/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            var log = HttpContext.Session.GetString("Log");

            if (log != "Admin")
                return RedirectToAction("Login", "Home");

            if (id == null)
            {
                return NotFound();
            }

            var task = await _client.GetAsync(_url + $"/{id}");
            var streamPage = await task.Content.ReadAsStringAsync();

            var Professor = JsonConvert.DeserializeObject<Professor>(streamPage);

            if (Professor == null)
            {
                return NotFound();
            }

            return View(Professor);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var log = HttpContext.Session.GetString("Log");

            if (log != "Admin")
                return RedirectToAction("Login", "Home");

            var del = await _client.DeleteAsync(_url + $"/{id}");
            return RedirectToAction("Index");
        }

        public IActionResult Error()
        {
            var log = HttpContext.Session.GetString("Log");

            if (log != "Admin")
                return BadRequest();

            return View();
        }

        public IActionResult SendEmail(string email, string subject, string messageBody)
        {
            _emailSender.SendEmailAsync(email, subject, messageBody);
            return RedirectToAction("Index");
        }
    }
}
