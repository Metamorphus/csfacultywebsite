﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using Newtonsoft.Json;
using System.Net;
using FacultyLanding.Models;
using Microsoft.AspNetCore.Http;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Net.Http.Headers;
// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace FacultyLanding.Areas.Adm.Controllers
{
    [Area("Admin")]
    public class ContactController : Controller
    {
        private readonly string _url = "http://localhost:58585/api/contacts";
        private HttpClient _client;
        private IHostingEnvironment _environment;

        public ContactController(IHostingEnvironment environment)
        {
            _client = new HttpClient();
            _environment = environment;
        }
        // GET: /<controller>/
        public async Task<IActionResult> Index()
        {
            return RedirectToAction("Details", new { id = 1 });
        }


        public IActionResult Create()
        {
            //check if authorized
            var log = HttpContext.Session.GetString("Log");
            if (log != "Admin")
                return RedirectToAction("Login", "Home");

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ContactId,Building,City,Country,Email,Fax,Latitude,Longitude,Office,Street,Telephone1,Telephone2,Website,ZipCode")] Contact contact)
        {
            //check if authorized
            var log = HttpContext.Session.GetString("Log");
            if (log != "Admin")
                return RedirectToAction("Login", "Home");

            if (ModelState.IsValid)
            {
                HttpContent content = new StringContent(JsonConvert.SerializeObject(contact), Encoding.UTF8, "application/json");
                var post = await _client.PostAsync(_url, content);

                return RedirectToAction("Index");
            }
            return View(contact);
        }

        public async Task<IActionResult> Details(int? id)
        {

            //check if authorized
            var log = HttpContext.Session.GetString("Log");
            if (log != "Admin")
                return RedirectToAction("Login", "Home");

            if (id == null)
            {
                return NotFound();
            }

            var task = await _client.GetAsync(_url + $"/{id}");
            var streamPage = await task.Content.ReadAsStringAsync();

            var contact = JsonConvert.DeserializeObject<Contact>(streamPage);

            if (contact == null)
            {
                return NotFound();
            }

            return View(contact);
        }

        public async Task<IActionResult> Edit(int? id)
        {
            //check if authorized
            var log = HttpContext.Session.GetString("Log");
            if (log != "Admin")
                return RedirectToAction("Login", "Home");

            if (id == null)
            {
                return NotFound();
            }
            var task = await _client.GetAsync(_url + $"/{id}");
            var streamPage = await task.Content.ReadAsStringAsync();

            var contact = JsonConvert.DeserializeObject<Contact>(streamPage);

            if (contact == null)
            {
                return NotFound();
            }
            return View(contact);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ContactId,Building,City,Country,Email,Fax,Latitude,Longitude,Office,Street,Telephone1,Telephone2,Website,ZipCode")] Contact contact)
        {
            //check if authorized
            var log = HttpContext.Session.GetString("Log");
            if (log != "Admin")
                return RedirectToAction("Login", "Home");

            if (id != contact.ContactId)
            {
                return NotFound();
            }
            if (ModelState.IsValid)
            {
                try
                {
                    _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    HttpContent content = new StringContent(JsonConvert.SerializeObject(contact), Encoding.UTF8, "application/json");
                    var put = await _client.PutAsync(_url + $"/{id}", content);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (contact.ContactId == 0)
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            return View(contact);
        }

        public async Task<IActionResult> Delete(int? id)
        {
            //check if authorized
            var log = HttpContext.Session.GetString("Log");
            if (log != "Admin")
                return RedirectToAction("Login", "Home");

            if (id == null)
            {
                return NotFound();
            }

            var task = await _client.GetAsync(_url + $"/{id}");
            var streamPage = await task.Content.ReadAsStringAsync();

            var contact = JsonConvert.DeserializeObject<Contact>(streamPage);

            if (contact == null)
            {
                return NotFound();
            }

            return View(contact);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            //check if authorized
            var log = HttpContext.Session.GetString("Log");
            if (log != "Admin")
                return RedirectToAction("Login", "Home");

            var del = await _client.DeleteAsync(_url + $"/{id}");
            return RedirectToAction("Index");
        }
    }
}
