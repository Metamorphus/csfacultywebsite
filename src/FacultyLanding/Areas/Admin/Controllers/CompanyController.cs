﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using Newtonsoft.Json;
using System.Net;
using FacultyLanding.Models;
using Microsoft.AspNetCore.Http;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Net.Http.Headers;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace FacultyLanding.Areas.Adm.Controllers
{
    [Area("Admin")]
    public class CompanyController : Controller
    {
        private readonly string _url = "http://localhost:58585/api/companies";
        private HttpClient _client;
        private IHostingEnvironment _environment;

        public CompanyController(IHostingEnvironment environment)
        {
            _client = new HttpClient();
            _environment = environment;
        }

        // GET: /<controller>/
        public async Task<IActionResult> Index()
        {
            var log = HttpContext.Session.GetString("Log");

            if (log != "Admin")
                return RedirectToAction("Login", "Home");

            var task = await _client.GetAsync(_url);
            var stringNews = await task.Content.ReadAsStringAsync();
            var Companies = (JsonConvert.DeserializeObject<IEnumerable<Company>>(stringNews)).ToList();

            return View(Companies);
        }


        public IActionResult Create()
        {
            var log = HttpContext.Session.GetString("Log");

            if (log != "Admin")
                return RedirectToAction("Login", "Home");

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("CompanyId,WebsiteLink,LogoLink")] Company Company, IFormFile file)
        {
            var log = HttpContext.Session.GetString("Log");

            if (log != "Admin")
                return RedirectToAction("Login", "Home");

            if (!Directory.Exists(_environment.WebRootPath + "/" + "upload/companies/"))
                Directory.CreateDirectory(_environment.WebRootPath + "/" + "upload/companies/");

            if (file == null)
            {
                Company.LogoLink = "https://storage.googleapis.com/prestashop/img/no_image.png";
            }
            else
            {
                List<string> validExtensions = new List<string> { ".jpg", ".png", ".jpeg" };
                var extension = Path.GetExtension(file.FileName);
                if (!validExtensions.Contains(extension))
                {
                    ModelState.AddModelError(String.Empty, "Valid extensions are .jpg, .png, .jpeg");
                    return View(Company);
                }
                Company.LogoLink = String.Concat("upload/professors/", file.FileName);

                using (var fileStream = new FileStream(
                Path.Combine(_environment.WebRootPath, Company.LogoLink), FileMode.Create))
                {
                    await file.CopyToAsync(fileStream);
                }
            }

            ModelState.Clear();
            this.TryValidateModel(Company);

            if (ModelState.IsValid)
            {
                HttpContent content = new StringContent(JsonConvert.SerializeObject(Company), Encoding.UTF8, "application/json");
                var post = await _client.PostAsync(_url, content);

                return RedirectToAction("Index");
            }
            return View(Company);
        }

        public async Task<IActionResult> Details(int? id)
        {
            var log = HttpContext.Session.GetString("Log");

            if (log != "Admin")
                return RedirectToAction("Login", "Home");

            if (id == null)
            {
                return NotFound();
            }

            var task = await _client.GetAsync(_url + $"/{id}");
            var streamPage = await task.Content.ReadAsStringAsync();

            var Company = JsonConvert.DeserializeObject<Company>(streamPage);

            if (Company == null)
            {
                return NotFound();
            }

            return View(Company);
        }

        public async Task<IActionResult> Edit(int? id)
        {
            var log = HttpContext.Session.GetString("Log");

            if (log != "Admin")
                return RedirectToAction("Login", "Home");

            if (id == null)
            {
                return NotFound();
            }
            var task = await _client.GetAsync(_url + $"/{id}");
            var streamPage = await task.Content.ReadAsStringAsync();

            var Company = JsonConvert.DeserializeObject<Company>(streamPage);

            if (Company == null)
            {
                return NotFound();
            }
            return View(Company);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("CompanyId,WebsiteLink,LogoLink")] Company Company, IFormFile file)
        {
            var log = HttpContext.Session.GetString("Log");

            if (log != "Admin")
                return RedirectToAction("Login", "Home");

            if (id != Company.CompanyId)
            {
                return NotFound();
            }
            if (file != null)
            {
                List<string> validExtensions = new List<string> { ".jpg", ".png", ".jpeg" };
                var extension = Path.GetExtension(file.FileName);
                if (!validExtensions.Contains(extension))
                {
                    ModelState.AddModelError(String.Empty, "Valid extensions are .jpg, .png, .jpeg");
                    return View(Company);
                }
                Company.LogoLink = String.Concat("upload/professors/", file.FileName);

                using (var fileStream = new FileStream(
                Path.Combine(_environment.WebRootPath, Company.LogoLink), FileMode.Create))
                {
                    await file.CopyToAsync(fileStream);
                }
            }

            ModelState.Clear();
            this.TryValidateModel(Company);

            if (ModelState.IsValid)
            {
                try
                {
                    _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    HttpContent content = new StringContent(JsonConvert.SerializeObject(Company), Encoding.UTF8, "application/json");
                    var put = await _client.PutAsync(_url + $"/{id}", content);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (Company.CompanyId == 0)
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            return View(Company);
        }

        public async Task<IActionResult> Delete(int? id)
        {
            var log = HttpContext.Session.GetString("Log");

            if (log != "Admin")
                return RedirectToAction("Login", "Home");

            if (id == null)
            {
                return NotFound();
            }

            var task = await _client.GetAsync(_url + $"/{id}");
            var streamPage = await task.Content.ReadAsStringAsync();

            var Company = JsonConvert.DeserializeObject<Company>(streamPage);

            if (Company == null)
            {
                return NotFound();
            }

            return View(Company);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var log = HttpContext.Session.GetString("Log");

            if (log != "Admin")
                return RedirectToAction("Login", "Home");

            var del = await _client.DeleteAsync(_url + $"/{id}");
            return RedirectToAction("Index");
        }

        public async Task<IActionResult> UnicityCheck([Bind("CompanyId,WebsiteLink,LogoLink")] Company company)
        {
            var task = await _client.GetAsync(_url);
            var stringNews = await task.Content.ReadAsStringAsync();
            var Companies = (JsonConvert.DeserializeObject<IEnumerable<Company>>(stringNews)).ToList();

            var cmp = Companies.Single(m => m.WebsiteLink == company.WebsiteLink);

            if (cmp != null)
                return Json(data: $"Url {company.WebsiteLink} already exists.");
            return Json(data: true);
        }
    }
}
