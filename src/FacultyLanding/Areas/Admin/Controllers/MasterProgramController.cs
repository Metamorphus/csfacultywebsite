﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using Newtonsoft.Json;
using System.Net;
using FacultyLanding.Models;
using Microsoft.AspNetCore.Http;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Net.Http.Headers;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace FacultyLanding.Areas.Adm.Controllers
{
    [Area("Admin")]
    public class MasterProgramController : Controller
    {
        private readonly string _url = "http://localhost:58585/api/masterprograms";
        private HttpClient _client;
        private IHostingEnvironment _environment;

        public MasterProgramController(IHostingEnvironment environment)
        {
            _client = new HttpClient();
            _environment = environment;
        }
        // GET: /<controller>/
        public async Task<IActionResult> Index()
        {
            var log = HttpContext.Session.GetString("Log");

            if (log != "Admin")
                return RedirectToAction("Login", "Home");

            var task = await _client.GetAsync(_url);
            var stringNews = await task.Content.ReadAsStringAsync();
            var mps = (JsonConvert.DeserializeObject<IEnumerable<MasterProgram>>(stringNews)).ToList();

            return View(mps);
        }


        public async Task<IActionResult> Create()
        {
            var log = HttpContext.Session.GetString("Log");

            if (log != "Admin")
                return RedirectToAction("Login", "Home");

            var task = await _client.GetAsync("http://localhost:58585/api/professors");
            var stringNews = await task.Content.ReadAsStringAsync();
            var profs = (JsonConvert.DeserializeObject<IEnumerable<Professor>>(stringNews)).ToList();
            ViewData["ProfessorId"] = new SelectList(profs, "ProfessorId","Name");

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("MpId,Code,Name,Desscription,ProfessorId")] MasterProgram mp)
        {
            var log = HttpContext.Session.GetString("Log");

            if (log != "Admin")
                return RedirectToAction("Login", "Home");

            if (ModelState.IsValid)
            {
                HttpContent content = new StringContent(JsonConvert.SerializeObject(mp), Encoding.UTF8, "application/json");
                var post = await _client.PostAsync(_url, content);

                return RedirectToAction("Index");
            }
            
            return View(mp);
        }

        public async Task<IActionResult> Details(int? id)
        {
            var log = HttpContext.Session.GetString("Log");

            if (log != "Admin")
                return RedirectToAction("Login", "Home");

            if (id == null)
            {
                return NotFound();
            }

            var task = await _client.GetAsync(_url + $"/{id}");
            var streamPage = await task.Content.ReadAsStringAsync();

            var mp = JsonConvert.DeserializeObject<MasterProgram>(streamPage);

            if (mp == null)
            {
                return NotFound();
            }

            return View(mp);
        }

        public async Task<IActionResult> Edit(int? id)
        {
            var log = HttpContext.Session.GetString("Log");

            if (log != "Admin")
                return RedirectToAction("Login", "Home");

            if (id == null)
            {
                return NotFound();
            }
            var task = await _client.GetAsync(_url + $"/{id}");
            var streamPage = await task.Content.ReadAsStringAsync();

            var mp = JsonConvert.DeserializeObject<MasterProgram>(streamPage);

            if (mp == null)
            {
                return NotFound();
            }

            var task2 = await _client.GetAsync("http://localhost:58585/api/professors");
            var stringNews = await task2.Content.ReadAsStringAsync();
            var profs = (JsonConvert.DeserializeObject<IEnumerable<Professor>>(stringNews)).ToList();
            ViewData["ProfessorId"] = new SelectList(profs, "ProfessorId", "Name");

            return View(mp);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("MpId,Code,Name,Desscription,ProfessorId")] MasterProgram mp)
        {
            var log = HttpContext.Session.GetString("Log");

            if (log != "Admin")
                return RedirectToAction("Login", "Home");

            if (id != mp.MpId)
            {
                return NotFound();
            }
            if (ModelState.IsValid)
            {
                try
                {
                    _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    HttpContent content = new StringContent(JsonConvert.SerializeObject(mp), Encoding.UTF8, "application/json");
                    var put = await _client.PutAsync(_url + $"/{id}", content);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (mp.MpId == 0)
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }   
                }
                return RedirectToAction("Index");
            }
            
            return View(mp);
        }

        public async Task<IActionResult> Delete(int? id)
        {
            var log = HttpContext.Session.GetString("Log");

            if (log != "Admin")
                return RedirectToAction("Login", "Home");

            if (id == null)
            {
                return NotFound();
            }

            var task = await _client.GetAsync(_url + $"/{id}");
            var streamPage = await task.Content.ReadAsStringAsync();

            var mp = JsonConvert.DeserializeObject<MasterProgram>(streamPage);

            if (mp == null)
            {
                return NotFound();
            }

            return View(mp);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var log = HttpContext.Session.GetString("Log");

            if (log != "Admin")
                return RedirectToAction("Login", "Home");

            var del = await _client.DeleteAsync(_url + $"/{id}");
            return RedirectToAction("Index");
        }
    }
}
