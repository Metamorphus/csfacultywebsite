﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using Newtonsoft.Json;
using System.Net;
using FacultyLanding.Models;
using Microsoft.AspNetCore.Http;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Net.Http.Headers;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace FacultyLanding.Areas.Adm.Controllers
{
    [Area("Admin")]
    public class FactController : Controller
    {
        private readonly string _url = "http://localhost:58585/api/facts";
        private HttpClient _client;
        private IHostingEnvironment _environment;

        public FactController(IHostingEnvironment environment)
        {
            _client = new HttpClient();
            _environment = environment;
        }

        // GET: /<controller>/
        public async Task<IActionResult> Index()
        {
            var log = HttpContext.Session.GetString("Log");

            if (log != "Admin")
                return RedirectToAction("Login","Home");
            var task = await _client.GetAsync(_url);
            var stringNews = await task.Content.ReadAsStringAsync();
            var facts = (JsonConvert.DeserializeObject<IEnumerable<Fact>>(stringNews)).ToList();

            return View(facts);
        }


        public IActionResult Create()
        {
            var log = HttpContext.Session.GetString("Log");

            if (log != "Admin")
                return RedirectToAction("Login", "Home");

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("FactId,Pic,Content")] Fact fact, IFormFile file)
        {
            var log = HttpContext.Session.GetString("Log");

            if (log != "Admin")
                return RedirectToAction("Login", "Home");

            if (!Directory.Exists(_environment.WebRootPath + "/" + "upload/facts/"))
                Directory.CreateDirectory(_environment.WebRootPath + "/" + "upload/facts/");

            if (file == null)
            {
                fact.Pic = "https://storage.googleapis.com/prestashop/img/no_image.png";
            }
            else
            {
                List<string> validExtensions = new List<string> { ".jpg", ".png", ".jpeg" };
                var extension = Path.GetExtension(file.FileName);
                if (!validExtensions.Contains(extension))
                {
                    ModelState.AddModelError(String.Empty, "Valid extensions are .jpg, .png, .jpeg");
                    return View(fact);
                }
                fact.Pic = String.Concat("upload/professors/", file.FileName);

                using (var fileStream = new FileStream(
                Path.Combine(_environment.WebRootPath, fact.Pic), FileMode.Create))
                {
                    await file.CopyToAsync(fileStream);
                }
            }

            ModelState.Clear();
            this.TryValidateModel(fact);

            if (ModelState.IsValid)
            {
                HttpContent content = new StringContent(JsonConvert.SerializeObject(fact), Encoding.UTF8, "application/json");
                var post = await _client.PostAsync(_url, content);

                return RedirectToAction("Index");
            }
            return View(fact);
        }

        public async Task<IActionResult> Details(int? id)
        {
            var log = HttpContext.Session.GetString("Log");

            if (log != "Admin")
                return RedirectToAction("Login", "Home");

            if (id == null)
            {
                return NotFound();
            }

            var task = await _client.GetAsync(_url + $"/{id}");
            var streamPage = await task.Content.ReadAsStringAsync();

            var fact = JsonConvert.DeserializeObject<Fact>(streamPage);

            if (fact == null)
            {
                return NotFound();
            }

            return View(fact);
        }

        public async Task<IActionResult> Edit(int? id)
        {
            var log = HttpContext.Session.GetString("Log");

            if (log != "Admin")
                return RedirectToAction("Login", "Home");

            if (id == null)
            {
                return NotFound();
            }
            var task = await _client.GetAsync(_url + $"/{id}");
            var streamPage = await task.Content.ReadAsStringAsync();

            var fact = JsonConvert.DeserializeObject<Fact>(streamPage);

            if (fact == null)
            {
                return NotFound();
            }
            return View(fact);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("FactId,Pic,Content")] Fact fact, IFormFile file)
        {
            var log = HttpContext.Session.GetString("Log");

            if (log != "Admin")
                return RedirectToAction("Login", "Home");

            if (id != fact.FactId)
            {
                return NotFound();
            }
            if (file != null)
            {
                List<string> validExtensions = new List<string> { ".jpg", ".png", ".jpeg" };
                var extension = Path.GetExtension(file.FileName);
                if (!validExtensions.Contains(extension))
                {
                    ModelState.AddModelError(String.Empty, "Valid extensions are .jpg, .png, .jpeg");
                    return View(fact);
                }
                fact.Pic = String.Concat("upload/professors/", file.FileName);

                using (var fileStream = new FileStream(
                Path.Combine(_environment.WebRootPath, fact.Pic), FileMode.Create))
                {
                    await file.CopyToAsync(fileStream);
                }
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    HttpContent content = new StringContent(JsonConvert.SerializeObject(fact), Encoding.UTF8, "application/json");
                    var put = await _client.PutAsync(_url + $"/{id}", content);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (fact.FactId == 0)
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            return View(fact);
        }

        public async Task<IActionResult> Delete(int? id)
        {
            var log = HttpContext.Session.GetString("Log");

            if (log != "Admin")
                return RedirectToAction("Login", "Home");

            if (id == null)
            {
                return NotFound();
            }

            var task = await _client.GetAsync(_url + $"/{id}");
            var streamPage = await task.Content.ReadAsStringAsync();

            var fact = JsonConvert.DeserializeObject<Fact>(streamPage);

            if (fact == null)
            {
                return NotFound();
            }

            return View(fact);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var del = await _client.DeleteAsync(_url + $"/{id}");
            return RedirectToAction("Index");
        }
  
    }
}
