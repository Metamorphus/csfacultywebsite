﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Net.Http;
using Microsoft.AspNetCore.Hosting;
using FacultyLanding.Models;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Http;
using System.IO;
using System.Text;
using System.Net.Http.Headers;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace FacultyLanding.Areas.Adm.Controllers
{
    [Area("Admin")]
    public class ReviewController : Controller
    {
        private readonly string _url = "http://localhost:58585/api/reviews";
        private HttpClient _client;
        private IHostingEnvironment _environment;
        private readonly ILogger<ReviewController> _logger;

        public ReviewController(IHostingEnvironment environment,
            ILogger<ReviewController> logger)
        {
            _client = new HttpClient();
            _environment = environment;
            _logger = logger;
        }

        // GET: NEWS
        public async Task<IActionResult> Index()
        {
            var log = HttpContext.Session.GetString("Log");

            if (log != "Admin")
                return RedirectToAction("Login", "Home");

            var task = await _client.GetAsync(_url);
            var stringResponse = await task.Content.ReadAsStringAsync();
            var reviews = JsonConvert.DeserializeObject<IEnumerable<Review>>(stringResponse).ToList();

            return View(reviews);
        }

        public IActionResult Create()
        {
            var log = HttpContext.Session.GetString("Log");

            if (log != "Admin")
                return RedirectToAction("Login", "Home");

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ReviewId,Title,Author,Content,PhotoLink")] Review review, IFormFile file)
        {
            var log = HttpContext.Session.GetString("Log");

            if (log != "Admin")
                return RedirectToAction("Login", "Home");

            if (!Directory.Exists(_environment.WebRootPath + "/" + "upload/reviews/"))
                Directory.CreateDirectory(_environment.WebRootPath + "/" + "upload/reviews/");

            if (file == null)
            {
                review.PhotoLink = "https://storage.googleapis.com/prestashop/img/no_image.png";
            }
            else
            {
                List<string> validExtensions = new List<string> { ".jpg", ".png", ".jpeg" };
                var extension = Path.GetExtension(file.FileName);
                if (!validExtensions.Contains(extension))
                {
                    ModelState.AddModelError(String.Empty, "Valid extensions are .jpg, .png, .jpeg");
                    return View(review);
                }
                review.PhotoLink = String.Concat("upload/professors/", file.FileName);

                using (var fileStream = new FileStream(
                Path.Combine(_environment.WebRootPath, review.PhotoLink), FileMode.Create))
                {
                    await file.CopyToAsync(fileStream);
                }
            }

            ModelState.Clear();
            this.TryValidateModel(review);

            if (ModelState.IsValid)
            {
                HttpContent content = new StringContent(JsonConvert.SerializeObject(review), Encoding.UTF8, "application/json");
                var post = await _client.PostAsync("http://localhost:58585/api/Reviews", content);

                return RedirectToAction("Index");
            }
            return View(review);
        }

        // GET: Review/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            var log = HttpContext.Session.GetString("Log");

            if (log != "Admin")
                return RedirectToAction("Login", "Home");

            if (id == null)
            {
                return NotFound();
            }

            var task = await _client.GetAsync(_url + $"/{id}");
            var streamPage = await task.Content.ReadAsStringAsync();

            var review = JsonConvert.DeserializeObject<Review>(streamPage);

            if (review == null)
            {
                return NotFound();
            }

            return View(review);
        }

        // GET: Review/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            var log = HttpContext.Session.GetString("Log");

            if (log != "Admin")
                return RedirectToAction("Login", "Home");

            if (id == null)
            {
                return NotFound();
            }
            var task = await _client.GetAsync(_url + $"/{id}");
            var streamPage = await task.Content.ReadAsStringAsync();

            var review = JsonConvert.DeserializeObject<Review>(streamPage);

            if (review == null)
            {
                return NotFound();
            }
            return View(review);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ReviewId,Title,Author,Content,PhotoLink")] Review review, IFormFile file)
        {
            var log = HttpContext.Session.GetString("Log");

            if (log != "Admin")
                return RedirectToAction("Login", "Home");

            if (id != review.ReviewId)
            {
                return NotFound();
            }
            if (file != null)
            {
                List<string> validExtensions = new List<string> { ".jpg", ".png", ".jpeg" };
                var extension = Path.GetExtension(file.FileName);
                if (!validExtensions.Contains(extension))
                {
                    ModelState.AddModelError(String.Empty, "Valid extensions are .jpg, .png, .jpeg");
                    return View(review);
                }
                review.PhotoLink = String.Concat("upload/professors/", file.FileName);

                using (var fileStream = new FileStream(
                Path.Combine(_environment.WebRootPath, review.PhotoLink), FileMode.Create))
                {
                    await file.CopyToAsync(fileStream);
                }
            }

            ModelState.Clear();
            this.TryValidateModel(review);

            if (ModelState.IsValid)
            {
                try
                {
                    _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    HttpContent content = new StringContent(JsonConvert.SerializeObject(review), Encoding.UTF8, "application/json");
                    var put = await _client.PutAsync(_url + $"/{id}", content);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (review.ReviewId == 0)
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            return View(review);
        }

        // GET: Review/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            var log = HttpContext.Session.GetString("Log");

            if (log != "Admin")
                return RedirectToAction("Login", "Home");

            if (id == null)
            {
                return NotFound();
            }

            var task = await _client.GetAsync(_url + $"/{id}");
            var streamPage = await task.Content.ReadAsStringAsync();

            var review = JsonConvert.DeserializeObject<Review>(streamPage);

            if (review == null)
            {
                return NotFound();
            }

            return View(review);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var log = HttpContext.Session.GetString("Log");

            if (log != "Admin")
                return RedirectToAction("Login", "Home");

            var del = await _client.DeleteAsync(_url + $"/{id}");
            return RedirectToAction("Index");
        }

        public IActionResult Error()
        {
            var log = HttpContext.Session.GetString("Log");

            if (log != "Admin")
                return RedirectToAction("Login", "Home");

            return View();
        }
    }
}
