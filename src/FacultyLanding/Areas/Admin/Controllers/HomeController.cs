﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using Newtonsoft.Json;
using System.Net;
using FacultyLanding.Models;
using Microsoft.AspNetCore.Http;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Net.Http.Headers;
using Microsoft.AspNetCore.Identity;
using System.Security.Cryptography;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace FacultyLanding.Areas.Adm.Controllers
{
    [Area("Admin")]
    public class HomeController : Controller
    {
        private readonly string _url = "http://localhost:58585/api/adm";
        private HttpClient _client;
        public readonly FacultyContext _context;

        public HomeController(FacultyContext context)
        {
            _client = new HttpClient();
            _context = context;
        }
        // GET: /<controller>/
        public IActionResult Index()
        {
            var log = HttpContext.Session.GetString("Log");

            if (log != "Admin")
                return RedirectToAction("Login");

            if (!HttpContext.Session.IsAvailable)
                return RedirectToAction("Error");

            var adm = _context.Adm.First();
            
            return View(adm);
        }

        [HttpGet]
        public IActionResult Login()
        {
            return View();
        }

        public string CalculateMD5Hash(string input)
        {
            MD5 md5 = MD5.Create();
            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
            byte[] hash = md5.ComputeHash(inputBytes);

            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("x2"));
            }

            return sb.ToString();
        }



        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Login([Bind("Login,Password")] Admin adm)
        {
            if (ModelState.IsValid)
            {
                var admin = _context.Adm.First();

                var hashPass = CalculateMD5Hash(adm.Password);

                if (adm.Login == admin.Login && hashPass == admin.Password)
                {
                    HttpContext.Session.SetString("Log", adm.Login);
                    HttpContext.Session.SetString("Pass", hashPass);
                    return RedirectToAction("Index","Fact");
                } else {
                    ModelState.AddModelError("", "Wrong username and/or password");
                }
            }
            return View(adm);
        }

        public IActionResult LogOff()
        {
            HttpContext.Session.Clear();

            return RedirectToAction("Login");
        }

        public IActionResult ChangePassword()
        {
            var adm = _context.Adm.First();
            
            if (adm == null)
            {
                return RedirectToAction("Index");
            }
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ChangePassword(string login, [Bind("OldPassword", "NewPassword", "ConfirmPassword")] ChangePasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var adm = _context.Adm.First();

                    if (CalculateMD5Hash(model.OldPassword) == adm.Password)
                    {
                        if (model.NewPassword == model.ConfirmPassword)
                        {
                            var hashPass = CalculateMD5Hash(model.NewPassword);

                            HttpContext.Session.SetString("Log", adm.Login);
                            HttpContext.Session.SetString("Pass", hashPass);
                            
                            _context.Adm.First().Password = CalculateMD5Hash(model.NewPassword);
                            _context.SaveChanges();
                        }
                        else
                            return RedirectToAction("Error");
                    }
                    else
                        return RedirectToAction("Error");
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (login == null)
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            return View(model);
        }
        public string Error()
        {
            var error = "ERROR";
            return error;
        }

    }
}
