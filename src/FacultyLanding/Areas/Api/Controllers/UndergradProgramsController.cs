﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using FacultyLanding.Models;
using Microsoft.AspNetCore.Http;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace FacultyLanding.Areas.Api.Controllers
{
    [Area("Api")]
    [Route("api/undergradprograms")]
    public class UndergradProgramsController : Controller
    {
        private readonly IFacultyRepository _repository;

        public UndergradProgramsController(IFacultyRepository repo)
        {
            _repository = repo;
        }
        // GET: api/values
        [HttpGet]
        public IActionResult Get()
        {
            IEnumerable<UndergradProgram> ups = _repository.GetAllUP();
            return new JsonResult(ups);
        }

        // GET api/values/5
        [HttpGet("{id}", Name = "GetUndergradProgram")]
        public IActionResult Get(int id)
        {
            var uProgram = _repository.GetUP(id);
            if (uProgram == null || uProgram.UpId != id)
            {
                return NotFound();
            }
            return new JsonResult(uProgram);
        }

        // POST api/values
        [HttpPost]
        public IActionResult Post([FromBody]UndergradProgram up)
        {
            if (up == null || !ModelState.IsValid)
            {
                return BadRequest();
            }
            _repository.AddNewUP(up);
            return CreatedAtRoute("GetUndergradProgram",
                new { id = up.UpId }, up);
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody]UndergradProgram up)
        {
            if (up == null || !ModelState.IsValid)
            {
                return BadRequest();
            }
            up.UpId = id;

            var oldUProgram = _repository.GetUP(id);
            if (oldUProgram == null || oldUProgram.UpId != id)
            {
                return NotFound();
            }

            _repository.UpdateUP(up);
            return new NoContentResult();
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var up = _repository.GetUP(id);
            if (up == null || up.UpId != id)
            {
                return NotFound();
            }

            _repository.RemoveUP(id);
            return new NoContentResult();
        }
    }
}
