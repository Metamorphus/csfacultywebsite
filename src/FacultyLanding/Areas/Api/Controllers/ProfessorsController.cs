﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using FacultyLanding.Models;
using Microsoft.AspNetCore.Http;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace FacultyLanding.Areas.Api.Controllers
{
    [Area("Api")]
    [Route("api/professors")]
    public class ProfessorsController : Controller
    {
        private readonly IFacultyRepository _repository;

        public ProfessorsController(IFacultyRepository repo)
        {
            _repository = repo;
        }

        // GET: api/values
        [HttpGet]
        public IActionResult Get()
        {
            IEnumerable<Professor> profs = _repository.GetAllProfessors();
            return new JsonResult(profs);
        }

        // GET api/values/5
        [HttpGet("{id}", Name = "GetProfessor")]
        public IActionResult Get(int id)
        {
            var prof = _repository.GetProfessor(id);
            if (prof == null || prof.ProfessorId != id)
            {
                return NotFound();
            }
            return new JsonResult(prof);
        }

        // POST api/values
        [HttpPost]
        public IActionResult Post([FromBody]Professor prof)
        {
            if (prof == null || !ModelState.IsValid)
            {
                return BadRequest();
            }
            _repository.AddProfessor(prof);
            return CreatedAtRoute("GetProfessor",
                new { id = prof.ProfessorId }, prof);
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody]Professor prof)
        {
            if (prof == null || !ModelState.IsValid)
            {
                return BadRequest();
            }
            prof.ProfessorId = id;

            var oldProf = _repository.GetProfessor(id);
            if (oldProf == null || oldProf.ProfessorId != id)
            {
                return NotFound();
            }

            _repository.UpdateProfessor(prof);
            return new NoContentResult();
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var prof = _repository.GetProfessor(id);
            if (prof == null || prof.ProfessorId != id)
            {
                return NotFound();
            }

            _repository.RemoveProfessor(id);
            return new NoContentResult();
        }
    }
}
