﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using FacultyLanding.Models;
using Microsoft.AspNetCore.Http;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace FacultyLanding.Areas.Api.Controllers
{
    [Area("Api")]
    [Route("api/Reviews")]
    public class ReviewsController : Controller
    {
        private readonly IFacultyRepository _repository;

        public ReviewsController(IFacultyRepository repo)
        {
            _repository = repo;
        }

        // GET: api/values
        [HttpGet]
        public IActionResult Get()
        {
            IEnumerable<Review> news = _repository.GetAllReviews();
            return new JsonResult(news);
        }

        // GET api/values/5
        [HttpGet("{id}", Name = "GetReview")]
        public IActionResult Get(int id)
        {
            var Review = _repository.GetReview(id);
            if (Review == null || Review.ReviewId != id)
            {
                return NotFound();
            }
            return new JsonResult(Review);
        }

        // POST api/values
        [HttpPost]
        public IActionResult Post([FromBody]Review review)
        {
            if (review == null || !ModelState.IsValid)
            {
                return BadRequest();
            }
            _repository.AddReview(review);
            return CreatedAtRoute("GetReview",
                new { id = review.ReviewId }, review);
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody]Review review)
        {
            if (review == null || !ModelState.IsValid)
            {
                return BadRequest();
            }
            review.ReviewId = id;

            var oldReview = _repository.GetReview(id);
            if (oldReview == null || oldReview.ReviewId != id)
            {
                return NotFound();
            }

            _repository.UpdateReview(review);
            return new NoContentResult();
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var review = _repository.GetReview(id);
            if (review == null || review.ReviewId != id)
            {
                return NotFound();
            }

            _repository.RemoveReview(id);
            return new NoContentResult();
        }
    }
}
