using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using FacultyLanding.Models;
using FacultyLanding.Services;
using Microsoft.AspNetCore.Http;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace FacultyLanding.Areas.Api.Controllers
{
    [Area("Api")]
    [Route("api/mail")]
    public class MailController : Controller
    {

        public readonly IEmailSender _mailSender;

        public MailController(IEmailSender sender){
            this._mailSender=sender;
        }


        [HttpPost]
        public async Task<IActionResult> Send([FromBody] Mail mail)
        {
            System.Console.WriteLine(mail.Name+" "+mail.Email+"------------------------");
            try
            {
            await this._mailSender.SendEmailAsync(mail.Email,mail.Name,mail.Message);                
                
            }
            catch (System.Exception)
            {
                
                var res= new JsonResult(new {error="send message error"});
                res.StatusCode=500;
                    throw;
            }

            return Json("message correctly sended");

          
        }


        
    }
      
}
