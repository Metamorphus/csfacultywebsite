﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using FacultyLanding.Models;
using Microsoft.AspNetCore.Http;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace FacultyLanding.Areas.Api.Controllers
{
    [Area("Api")]
    [Route("api/contacts")]
    public class ContactsController : Controller
    {
        private readonly IFacultyRepository _repository;

        public ContactsController(IFacultyRepository repo)
        {
            _repository = repo;
        }
        // GET: api/values
        [HttpGet]
        public IActionResult Get()
        {
            IEnumerable<Contact> contacts = _repository.GetAllContacts();
            return new JsonResult(contacts);
        }

        // GET api/values/5
        [HttpGet("{id}", Name = "GetContact")]
        public IActionResult Get(int id)
        {
            var cotact = _repository.GetContact(id);
            if (cotact == null || cotact.ContactId != id)
            {
                return NotFound();
            }
            return new JsonResult(cotact);
        }

        // POST api/values
        [HttpPost]
        public IActionResult Post([FromBody]Contact contact)
        {
            if (contact == null || !ModelState.IsValid)
            {
                return BadRequest();
            }
            _repository.AddContact(contact);
            return CreatedAtRoute("GetContact",
                new { id = contact.ContactId }, contact);
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody]Contact contact)
        {
            if (contact == null || !ModelState.IsValid)
            {
                return BadRequest();
            }
            contact.ContactId = id;

            var oldContact = _repository.GetContact(id);
            if (oldContact == null || oldContact.ContactId != id)
            {
                return NotFound();
            }

            _repository.UpdateContact(contact);
            return new NoContentResult();
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var contact = _repository.GetContact(id);
            if (contact == null || contact.ContactId != id)
            {
                return NotFound();
            }

            _repository.RemoveContact(id);
            return new NoContentResult();
        }
    }
}
