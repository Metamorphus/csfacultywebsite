﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using FacultyLanding.Models;
using Microsoft.AspNetCore.Http;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace FacultyLanding.Areas.Api.Controllers
{
    [Area("Api")]
    [Route("api/newsitems")]
    public class NewsItemsController : Controller
    {
        private readonly IFacultyRepository _repository;

        public NewsItemsController(IFacultyRepository repo)
        {
            _repository = repo;
        }

        // GET: api/values
        [HttpGet]
        public IActionResult Get()
        {
            IEnumerable<NewsItem> news = _repository.GetAllNewsItems();
            return new JsonResult(news);
        }

        // GET api/values/5
        [HttpGet("{id}", Name = "GetNewsItem")]
        public IActionResult Get(int id)
        {
            var newsItem = _repository.GetNewsItem(id);
            if (newsItem == null || newsItem.NewsItemId != id)
            {
                return NotFound();
            }
            return new JsonResult(newsItem);
        }

        // POST api/values
        [HttpPost]
        public IActionResult Post([FromBody]NewsItem ni)
        {
            if (ni == null || !ModelState.IsValid)
            {
                return BadRequest();
            }
            _repository.AddNewsItem(ni);
            return CreatedAtRoute("GetNewsItem",
                new { id = ni.NewsItemId }, ni);
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody]NewsItem ni)
        {
            if (ni == null || !ModelState.IsValid)
            {
                return BadRequest();
            }
            ni.NewsItemId = id;

            var oldNewsItem = _repository.GetNewsItem(id);
            if (oldNewsItem == null || oldNewsItem.NewsItemId != id)
            {
                return NotFound();
            }

            _repository.UpdateNewsItem(ni);
            return new NoContentResult();
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var ni = _repository.GetNewsItem(id);
            if (ni == null || ni.NewsItemId != id)
            {
                return NotFound();
            }

            _repository.RemoveNewsItem(id);
            return new NoContentResult();
        }
    }
}
