﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using FacultyLanding.Models;
using Microsoft.AspNetCore.Http;
using System.Net;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace FacultyLanding.Areas.Api.Controllers
{
    [Area("Api")]
    [Route("api/companies")]
    public class CompaniesController : Controller
    {
        private readonly IFacultyRepository _repository;

        public CompaniesController(IFacultyRepository repo)
        {
            _repository = repo;
        }

        // GET: api/values
        [HttpGet]
        public IActionResult Get()
        {
            IEnumerable<Company> Companies = _repository.GetAllCompanies();
            JsonResult CompaniesJson = new JsonResult(Companies);
            return CompaniesJson;
        }

        // GET api/values/5
        [HttpGet("{id}", Name = "GetCompany")]
        public IActionResult Get([FromRoute] int id)
        {
            var item = _repository.GetCompany(id);
            if (item == null || item.CompanyId != id)
            {
                return NotFound();
            }
            return new JsonResult(item);
        }

        // POST api/values
        [HttpPost]
        public IActionResult Post([FromBody]Company item)
        {
            if (item == null || !ModelState.IsValid)
            {
                return BadRequest();
            }
            _repository.AddCompany(item);
            return CreatedAtRoute("GetCompany", new { id = item.CompanyId }, item);
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public IActionResult Put([FromRoute] int id, [FromBody]Company item)
        {
            if (item == null || !ModelState.IsValid)
            {
                return NotFound();
            }
            item.CompanyId = id;

            var oldCompany = _repository.GetCompany(id);
            if (oldCompany == null || oldCompany.CompanyId != id)
            {
                return NotFound();
            }

            _repository.UpdateCompany(item);
            return new NoContentResult();
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public IActionResult Delete([FromRoute] int id)
        {
            var todo = _repository.GetCompany(id);
            if (todo == null)
            {
                return NotFound();
            }

            _repository.RemoveCompany(todo.CompanyId);
            return new NoContentResult();
        }
    }
}
