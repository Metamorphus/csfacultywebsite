﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using FacultyLanding.Models;
using Microsoft.AspNetCore.Http;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace FacultyLanding.Areas.Api.Controllers
{
    [Area("Api")]
    [Route("api/masterprograms")]
    public class MasterProgramsController : Controller
    {
        private readonly IFacultyRepository _repository;

        public MasterProgramsController(IFacultyRepository repo)
        {
            _repository = repo;
        }
        // GET: api/values
        [HttpGet]
        public IActionResult Get()
        {
            IEnumerable<MasterProgram> mps = _repository.GetAllMP();
            return new JsonResult(mps);
        }

        // GET api/values/5
        [HttpGet("{id}", Name = "GetMasterProgram")]
        public IActionResult Get(int id)
        {
            var mProgram = _repository.GetMP(id);
            if (mProgram == null || mProgram.MpId != id)
            {
                return NotFound();
            }
            return new JsonResult(mProgram);
        }

        // POST api/values
        [HttpPost]
        public IActionResult Post([FromBody]MasterProgram mp)
        {
            if (mp == null || !ModelState.IsValid)
            {
                return BadRequest();
            }
            _repository.AddNewMP(mp);
            return CreatedAtRoute("GetMasterProgram",
                new { id = mp.MpId }, mp);
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody]MasterProgram mp)
        {
            if (mp == null || !ModelState.IsValid)
            {
                return BadRequest();
            }
            mp.MpId = id;

            var oldMProgram = _repository.GetMP(id);
            if (oldMProgram == null || oldMProgram.MpId != id)
            {
                return NotFound();
            }

            _repository.UpdateMP(mp);
            return new NoContentResult();
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var mp = _repository.GetMP(id);
            if (mp == null || mp.MpId != id)
            {
                return NotFound();
            }

            _repository.RemoveMP(id);
            return new NoContentResult();
        }
    }
}
