﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using FacultyLanding.Models;
using Microsoft.AspNetCore.Http;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace FacultyLanding.Areas.Api.Controllers
{
    [Area("Api")]
    [Route("api/facts")]
    public class FactsController : Controller
    {
        private readonly IFacultyRepository _repository;

        public FactsController(IFacultyRepository repo)
        {
            _repository = repo;
        }

        // GET: api/values
        [HttpGet]
        public IActionResult Get()
        {
            IEnumerable<Fact> facts = _repository.GetAllFacts();
            JsonResult factsJson = new JsonResult(facts);
            return factsJson;
        }

        // GET api/values/5
        [HttpGet("{id}", Name = "GetFact")]
        public IActionResult Get([FromRoute] int id)
        {
            var item = _repository.GetFact(id);
            if (item == null || item.FactId != id)
            {
                return NotFound();
            }
            return new JsonResult(item);
        }

        // POST api/values
        [HttpPost]
        public IActionResult Post([FromBody]Fact item)
        {
            if (item == null || !ModelState.IsValid)
            {
                return BadRequest();
            }
            _repository.AddFact(item);
            return CreatedAtRoute("GetFact", new { id = item.FactId }, item);
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public IActionResult Put([FromRoute] int id, [FromBody]Fact item)
        {
            if (item == null || !ModelState.IsValid)
            {
                return NotFound();
            }
            item.FactId = id;

            var oldFact = _repository.GetFact(id);
            if (oldFact == null || oldFact.FactId != id)
            {
                return NotFound();
            }

            _repository.UpdateFact(item);
            return new NoContentResult();
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public IActionResult Delete([FromRoute] int id)
        {
            var todo = _repository.GetFact(id);
            if (todo == null)
            {
                return NotFound();
            }

            _repository.RemoveFact(todo.FactId);
            return new NoContentResult();
        }
    }
}
