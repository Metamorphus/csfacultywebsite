﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MailKit.Net.Smtp;
using MailKit.Security;
using MimeKit;

namespace FacultyLanding.Services
{
    // This class is used by the application to send Email and SMS
    // when you turn on two-factor authentication in ASP.NET Identity.
    // For more details see this link http://go.microsoft.com/fwlink/?LinkID=532713
    public class AuthMessageSender : IEmailSender, ISmsSender
    {
        
            const string mailClientLogin = "malevolent.deer@gmail.com";
            const string mailClientPass = "GOOGLE_YA_TEBYA_NENAVIZHU";
            const string mailTo = "kraken@ex.ua";
        public async Task SendEmailAsync(string email, string subject, string messageBody)
        {


         

            var message = new MimeMessage();
            message.From.Add(new MailboxAddress(email, email));
            message.To.Add(new MailboxAddress(mailTo,mailTo));
            message.Subject = subject;

            message.Body = new TextPart("html") { Text = messageBody };

            using (var client = new SmtpClient())
            {
                await client.ConnectAsync("smtp.gmail.com", 465, SecureSocketOptions.SslOnConnect).ConfigureAwait(false);
                await client.AuthenticateAsync(mailClientLogin, mailClientPass);
                await client.SendAsync(message).ConfigureAwait(false);
                await client.DisconnectAsync(true).ConfigureAwait(false);
            }
        }

        public Task SendSmsAsync(string number, string message)
        {
            // Plug in your SMS service here to send a text message.
            return Task.FromResult(0);
        }
    }
}
