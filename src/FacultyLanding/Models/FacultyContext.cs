﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FacultyLanding.Models
{
    public class FacultyContext : IdentityDbContext<ApplicationUser>
    {
        public DbSet<Professor> Professors { get; set; }
        public DbSet<Fact> Facts { get; set; }
        public DbSet<NewsItem> NewsItems { get; set; }
        public DbSet<MasterProgram> MP { get; set; }
        public DbSet<UndergradProgram> UP { get; set; }
        public DbSet<Review> Reviews { get; set; }
        public DbSet<Company> Companies { get; set; }
        public DbSet<Contact> Contacts { get; set; }
        public DbSet<Admin> Adm { get; set; }

        public FacultyContext(DbContextOptions<FacultyContext> options)
            : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite("Filename=./faculty.db");
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<NewsItem>(entity =>
            {
                entity.Property(e => e.PublicationDate).HasDefaultValueSql("CURRENT_TIMESTAMP");
            });
        }
    }
}
