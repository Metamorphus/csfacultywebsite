﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace FacultyLanding.Models
{
    public interface IFacultyRepository 
    {
        IEnumerable<Professor> GetAllProfessors();
        Professor GetProfessor(int id);
        void AddProfessor(Professor prof);
        void RemoveProfessor(int id);
        void UpdateProfessor(Professor prof);

        IEnumerable<Fact> GetAllFacts();
        Fact GetFact(int id);
        void AddFact(Fact prof);
        void RemoveFact(int id);
        void UpdateFact(Fact prof);

        IEnumerable<NewsItem> GetAllNewsItems();
        NewsItem GetNewsItem(int id);
        void AddNewsItem(NewsItem ni);
        void RemoveNewsItem(int id);
        void UpdateNewsItem(NewsItem ni);


        IEnumerable<UndergradProgram> GetAllUP();
        UndergradProgram GetUP(int id);
        void AddNewUP(UndergradProgram ni);
        void RemoveUP(int id);
        void UpdateUP(UndergradProgram ni);
        
        IEnumerable<MasterProgram> GetAllMP();
        MasterProgram GetMP(int id);
        void AddNewMP(MasterProgram mp);
        void RemoveMP(int id);
        void UpdateMP(MasterProgram mp);

        IEnumerable<Review> GetAllReviews();
        Review GetReview(int id);
        void AddReview(Review review);
        void RemoveReview(int id);
        void UpdateReview(Review review);

        IEnumerable<Company> GetAllCompanies();
        Company GetCompany(int id);
        void AddCompany(Company company);
        void RemoveCompany(int id);
        void UpdateCompany(Company company);

        IEnumerable<Contact> GetAllContacts();
        Contact GetContact(int id);
        void AddContact(Contact contact);
        void RemoveContact(int id);
        void UpdateContact(Contact contact);
    }
}
