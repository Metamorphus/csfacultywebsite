﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FacultyLanding.Models
{
    public class NewsItem
    {
        public int NewsItemId { get; set; }

        [Required]
        public string Title { get; set; }

        [Required]
        [MaxLength(256)]
        public string Snippet { get; set; }

        [Required]
        public string Content { get; set; }

        [Required]
        public string PhotoLink { get; set; }

        [Required]
        public DateTime PublicationDate { get; set; } 
    }

}
