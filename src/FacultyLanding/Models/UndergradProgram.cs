﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace FacultyLanding.Models
{
    public class UndergradProgram
    {
        [Key]
        public int UpId { get; set; }

        [Required]
        public string Code { get; set; }

        [Required, MaxLength(100)]
        public string Name { get; set; }

        [Required]
        public string Desscription { get; set; }

        public int ProfessorId { get; set; }

        public Professor Professor { get; set; }
    }
}
