﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace FacultyLanding.Models
{
    public class TitleAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            Professor prof = (Professor)validationContext.ObjectInstance;

            if (prof.Title != "Кандидат наук" && prof.Title != "Доктор наук")
            {
                return new ValidationResult("bad professor title");
            }

            return ValidationResult.Success;
        }
    }

    public class PositionAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            Professor prof = (Professor)validationContext.ObjectInstance;

            List<string> validPositions = new List<string> {
                "Асистент",
                "Старший викладач",
                "Доцент",
                "Професор"
            };

            if (!validPositions.Contains(prof.Position))
            {
                return new ValidationResult("bad professor position");
            }

            return ValidationResult.Success;
        }
    }

    public class Professor
    {
        public int ProfessorId { get; set; }

        [MaxLength(100)]
        [Required]
        public string Name { get; set; }
        [Required]
        [Title]
        public string Title { get; set; }
        [Required]
        [Position]
        public string Position { get; set; }

        
        public string PhotoLink { get; set; }

        [Required]
        public string Portfolio { get; set; }

        [Required]
        public int Rank { get; set; }

        [Phone]
        public string Phone { get; set; }
        [EmailAddress]
        public string Email { get; set; }
        [Url]
        public string Website { get; set; }

    }
}
