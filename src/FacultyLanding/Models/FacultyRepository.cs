﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FacultyLanding.Models
{
    public class FacultyRepository : IFacultyRepository
    {
        private FacultyContext _context = new FacultyContext(new DbContextOptions<FacultyContext>());


        public void AddCompany(Company company)
        {
            _context.Companies.Add(company);
            _context.SaveChanges();
            _context.Entry(company).State = EntityState.Detached;
        }

        public void AddContact(Contact contact)
        {
            _context.Contacts.Add(contact);
            _context.SaveChanges();
            _context.Entry(contact).State = EntityState.Detached;
        }

        public void AddFact(Fact fact)
        {
            _context.Facts.Add(fact);
            _context.SaveChanges();
            _context.Entry(fact).State = EntityState.Detached;
        }

        public void AddNewMP(MasterProgram mp)
        {
            _context.MP.Add(mp);
            _context.SaveChanges();
            _context.Entry(mp).State = EntityState.Detached;
        }

        public void AddNewsItem(NewsItem ni)
        {
            _context.NewsItems.Add(ni);
            _context.SaveChanges();
            _context.Entry(ni).State = EntityState.Detached;
        }

        public void AddNewUP(UndergradProgram up)
        {
            _context.UP.Add(up);
            _context.SaveChanges();
            _context.Entry(up).State = EntityState.Detached;
        }

        public void AddProfessor(Professor prof)
        {
            _context.Professors.Add(prof);
            _context.SaveChanges();
            _context.Entry(prof).State = EntityState.Detached;
        }

        public void AddReview(Review review)
        {
            _context.Reviews.Add(review);
            _context.SaveChanges();
            _context.Entry(review).State = EntityState.Detached;
        }

        public IEnumerable<Company> GetAllCompanies()
        {
            return _context.Companies.AsNoTracking();
        }

        public IEnumerable<Contact> GetAllContacts()
        {
            return _context.Contacts.AsNoTracking();
        }

        public IEnumerable<Fact> GetAllFacts()
        {
            return _context.Facts.AsNoTracking();
        }

        public IEnumerable<MasterProgram> GetAllMP()
        {
            return _context.MP.Include(p => p.Professor).AsNoTracking();
        }

        public IEnumerable<NewsItem> GetAllNewsItems()
        {
            return _context.NewsItems.AsNoTracking();
        }

        public IEnumerable<Professor> GetAllProfessors()
        {
            return _context.Professors.OrderBy(p => p.Rank).AsNoTracking();
        }

        public IEnumerable<Review> GetAllReviews()
        {
            return _context.Reviews.AsNoTracking();
        }

        public Company GetCompany(int id)
        {
            return _context.Companies.AsNoTracking().SingleOrDefault(c => c.CompanyId == id);
        }


        public Contact GetContact(int id)
        {
            return _context.Contacts.AsNoTracking().SingleOrDefault(c => c.ContactId == id);
        }

        public IEnumerable<UndergradProgram> GetAllUP()
        {
            return _context.UP.Include(p => p.Professor).AsNoTracking();
        }

        public Fact GetFact(int id)
        {
            return _context.Facts.AsNoTracking().SingleOrDefault( fact => fact.FactId == id);
        }

        public MasterProgram GetMP(int id)
        {
            var mprog = _context.MP.AsNoTracking().SingleOrDefault(mp => mp.MpId == id);
            mprog.Professor = GetProfessor(mprog.ProfessorId);
            return mprog;
        }

        public NewsItem GetNewsItem(int id)
        {
            return _context.NewsItems.AsNoTracking().SingleOrDefault(ni => ni.NewsItemId == id);
        }

        public Professor GetProfessor(int id)
        {
            return _context.Professors.AsNoTracking().SingleOrDefault(prof => prof.ProfessorId == id);
        }


        public UndergradProgram GetUP(int id)
        {
            var uprog = _context.UP.AsNoTracking().SingleOrDefault(up => up.UpId == id);
            uprog.Professor = GetProfessor(uprog.ProfessorId);
            return uprog;
        }
        public Review GetReview(int id)
        {
            return _context.Reviews.AsNoTracking().SingleOrDefault(r => r.ReviewId == id);
        }

        public void RemoveCompany(int id)
        {
            var company = _context.Companies.SingleOrDefault(c => c.CompanyId == id);
            _context.Companies.Remove(company);
            _context.SaveChanges();
        }

        public void RemoveContact(int id)
        {
            var contact = _context.Contacts.SingleOrDefault(c => c.ContactId == id);
            _context.Contacts.Remove(contact);
            _context.SaveChanges();
        }

        public void RemoveFact(int id)
        {
            var fact = _context.Facts.SingleOrDefault(fct => fct.FactId == id);
            _context.Facts.Remove(fact);
            _context.SaveChanges();
        }

        public void RemoveMP(int id)
        {
            var masterProg = _context.MP.SingleOrDefault(mp => mp.MpId == id);
            _context.MP.Remove(masterProg);
            _context.SaveChanges();
        }

        public void RemoveNewsItem(int id)
        {
            var newsitem = _context.NewsItems.SingleOrDefault(ni => ni.NewsItemId == id);
            _context.NewsItems.Remove(newsitem);
            _context.SaveChanges();
        }

        public void RemoveProfessor(int id)
        {
            var prof = _context.Professors.SingleOrDefault(pr => pr.ProfessorId == id);
            _context.Professors.Remove(prof);
            _context.SaveChanges();
        }


        public void RemoveUP(int id)
        {
            var undProg = _context.UP.SingleOrDefault(up => up.UpId == id);
            _context.UP.Remove(undProg);
            _context.SaveChanges();
        }


        public void RemoveReview(int id)
        {
            var review = _context.Reviews.SingleOrDefault(r => r.ReviewId == id);
            _context.Reviews.Remove(review);
            _context.SaveChanges();
        }

        public void UpdateCompany(Company company)
        {
            _context.Companies.Update(company);
            _context.SaveChanges();
            _context.Entry(company).State = EntityState.Detached;
        }

        public void UpdateContact(Contact contact)
        {
            _context.Contacts.Update(contact);
            _context.SaveChanges();
            _context.Entry(contact).State = EntityState.Detached;
        }

        public void UpdateFact(Fact fact)
        {
            _context.Facts.Update(fact);
            _context.SaveChanges();
            _context.Entry(fact).State = EntityState.Detached;
        }

        public void UpdateMP(MasterProgram mp)
        {
            _context.MP.Update(mp);
            _context.SaveChanges();
            _context.Entry(mp).State = EntityState.Detached;
        }

        public void UpdateNewsItem(NewsItem ni)
        {
            _context.NewsItems.Update(ni);
            _context.SaveChanges();
            _context.Entry(ni).State = EntityState.Detached;
        }

        public void UpdateProfessor(Professor prof)
        {
            _context.Professors.Update(prof);
            _context.SaveChanges();
            _context.Entry(prof).State = EntityState.Detached;
        }


        public void UpdateUP(UndergradProgram up)
        {
            _context.UP.Update(up);
            _context.SaveChanges();
            _context.Entry(up).State = EntityState.Detached;
        }
        public void UpdateReview(Review review)
        {
            _context.Reviews.Update(review);
            _context.SaveChanges();
            _context.Entry(review).State = EntityState.Detached;

        }
    }
}
