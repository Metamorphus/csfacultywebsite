using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace FacultyLanding.Models
{
    public static class SeedData
    {
        private static void AddAdmin(FacultyContext context)
        {
            context.Adm.AddRange(
                new Admin
                {
                    Login = "Admin",
                    Password = "5f4dcc3b5aa765d61d8327deb882cf99"
                }
            );
            context.SaveChanges();
        }

        private static void AddProfessors(FacultyContext context)
        {
            context.Professors.AddRange(
                new Professor
                {
                    ProfessorId = 1,
                    Name = "Микола Миколайович Глибовець",
                    Title = "доктор фізико-математичних наук, професор",
                    Position = "декан",
                    PhotoLink = "http://fin.ukma.edu.ua/wp-content/uploads/2016/04/IMG_5567-683x1024.jpg",
                    Portfolio = "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Excepturi voluptate quaerat quas accusantium, mollitia cumque? Nemo deleniti consequatur veniam consequuntur cumque nihil necessitatibus. Ea sed minima rerum at assumenda, omnis soluta et voluptates ratione fuga.",
                    Rank = 0
                },
                new Professor
                {
                    ProfessorId = 2,
                    Name = "Андрій Олександрович Афонін",
                    Title = "старший викладач",
                    Position = "заступник декана",
                    PhotoLink = "http://fin.ukma.edu.ua/wp-content/uploads/2016/04/IMG_5453-683x1024.jpg",
                    Portfolio = "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Excepturi voluptate quaerat quas accusantium, mollitia cumque? Nemo deleniti consequatur veniam consequuntur cumque nihil necessitatibus. Ea sed minima rerum at assumenda, omnis soluta et voluptates ratione fuga.",
                    Rank = 1,
                    Phone = "(044) 425 60 64"
                },
                  new Professor
                {
                    ProfessorId = 3,
                    Name = "Глибовець  Андрій   Миколайович",
                    Title = "старший викладач",
                    Position = "викладач",
                    PhotoLink = "http://fin.ukma.edu.ua/wp-content/uploads/2016/04/IMG_5476-683x1024.jpg",
                    Portfolio = "Магістр з інформаційних управляючих систем та технологій, доцент, аспірант, випускник НаУКМА",
                    Rank = 1,
                    Phone = "(044) 425 60 64"
                }
            );
            context.SaveChanges();
        }

        private static void AddFacts(FacultyContext context)
        {
            context.Facts.AddRange(
                new Fact
                {
                    Pic = "https://hd.unsplash.com/44/C3EWdWzT8imxs0fKeKoC_blackforrest.JPG",
                    Content = "Ut tempor tortor sollicitudin ligula lacinia dignissim. Proin eleifend tellus metus, elementum gravida dolor interdum non. Duis aliquet egestas velit, quis maximus odio maximus lacinia."
                },
                new Fact
                {
                    Pic = "https://hd.unsplash.com/photo-1423034816855-245e5820ff8c",
                    Content = "Donec enim metus, blandit at lectus non, ultricies mollis lectus. Praesent fermentum urna fermentum velit rutrum accumsan. Donec efficitur odio id nulla molestie feugiat."
                }
            );
            context.SaveChanges();
        }

        private static void AddNews(FacultyContext context)
        {
            context.NewsItems.AddRange(
                new NewsItem
                {
                    Title = "Преимущества systemd-networkd на виртуальных серверах Linux",
                    Snippet = "Обычно на десктопах Linux для управления сетевыми настройками используется NetworkManager, поскольку он отлично справляется со своей работой и имеет GUI фронтенды для всех популярных графических окружений. Однако на серверах Linux его использование не целесообразно: он потребляет много ресурсов. NetworkManager занимает в оперативной памяти около 20 Мб, в то время как systemd-networkd и systemd-resolvd вместе меньше 2 Мб. По этой причине, по умолчанию серверные дистрибутивы Linux часто используют различные собственные демоны.",
                    Content = "Обычно на десктопах Linux для управления сетевыми настройками используется NetworkManager, поскольку он отлично справляется со своей работой и имеет GUI фронтенды для всех популярных графических окружений. Однако на серверах Linux его использование не целесообразно: он потребляет много ресурсов. NetworkManager занимает в оперативной памяти около 20 Мб, в то время как systemd-networkd и systemd-resolvd вместе меньше 2 Мб. По этой причине, по умолчанию серверные дистрибутивы Linux часто используют различные собственные демоны. Таким образом возникает целый зоопарк скриптов и утилит: демон networking под Debian, который управляет конфигурацией сети через ifupdown, использующий файлы конфигурации хранящиеся в /etc/networing/interfaces.d и файл /etc/networking/interfaces, под CentOS network, который использует скрипты ifup и ifdown и, конечно же, свои файлы конфигурации находящиеся в /etc/sysconfig/network-scripts, netctl под ArchLinux. Всем известно, что Linux — конструктор, но почему бы такой простой и общей для самых различных систем вещи как настройка сети не иметь одинаковый вид? Мы предлагаем начать использовать быстрый и простой демон systemd-networkd, особенно в свете того, что многие дистрибутивы уже перешли на systemd, поэтому переключение на systemd-networkd не составит труда. На текущий момент systemd-networkd может заменить собой множество утилит и поддерживает, настройку сети как по DHCP (клиент и сервер) так и со статическими IP-адресами, мосты, туннели, VLANs, беспроводные сети (используя при этом wpa_supplicant). В статье мы рассмотрим, как активировать systemd-networkd и начать его использовать и в чем его основные преимущества перед остальными демонами.",
                    PhotoLink = "https://hsto.org/files/fd0/889/577/fd088957790e4dac8d74c96807af3185.png",
                    PublicationDate = new DateTime(2016, 08, 30)
                },
                new NewsItem
                {
                    Title = "Baidu опубликовала демо инструмента глубинного обучения PaddlePaddle",
                    Snippet = "Китайский поисковый гигант Baidu опубликовал демо-версию исходного кода своего инструмента глубинного обучения PaddlePaddle (PArallel Distributed Deep LEarning) на GitHub. Анонс всего кода PaddlePaddle состоится 30 сентября.",
                    Content = "Китайский поисковый гигант Baidu опубликовал демо-версию исходного кода своего инструмента глубинного обучения PaddlePaddle (PArallel Distributed Deep LEarning) на GitHub. Анонс всего кода PaddlePaddle состоится 30 сентября. Согласно информации на GitHub, сейчас сборка PaddlePaddle сырая и к массовому «употреблению» заинтересованными лицами готова не до конца. Разработчики заранее предупреждают, что еще не все файлы и пакеты готовы к установке, поэтому у желающих опробовать публичную демо-версию могут возникнуть серьезные проблемы при попытке работы с PaddlePaddle. Китайцы отмечают следующие особенности своей разработки: гибкость; высокий КПД и оптимизация использования мощностей; масштабируемость; совместимость с другими продуктами. По заявлению разработчиков, PaddlePaddle проще в использовании, чем аналоги от Amazon (DSSTN), Google (TensorFlow) и Microsoft (CNTK), которые также были опубликованы на GitHub для общего пользования. Эти инструменты, как считают китайцы, имеют высокий порог вхождения для сторонних разработчиков из-за своей массивности и многофункциональности. PaddlePadddle же проще, управляется при помощи C++ или Python. В качестве наглядного примера приводят размер кода: для решения одних и тех же задач код для Pubble будет в четыре раза короче, чем для разработок конкурентов.",
                    PhotoLink = "https://hsto.org/getpro/habr/post_images/d46/5ff/5c8/d465ff5c80609a9d042754bdc3733797.jpg",
                    PublicationDate = new DateTime(2016, 09, 01)
                },
                new NewsItem
                {
                    Title = "Табы или пробелы? Анализ 400 тысяч репозиториев GitHub, миллиарда файлов, 14 ТБ кода",
                    Snippet = "Для пытливых разработчиков до сих пор остается актуальным вопрос использования табуляции и пробелов для форматирования кода. Могут ли они быть взаимозаменяемы: например, 2 пробела на табуляцию или 4? Но единого стандарта нет, поэтому иногда между разработчиками возникает непонимание. Кроме того, различные IDE и их компиляторы обрабатывают табуляцию также по-своему.",
                    Content = "Для пытливых разработчиков до сих пор остается актуальным вопрос использования табуляции и пробелов для форматирования кода. Могут ли они быть взаимозаменяемы: например, 2 пробела на табуляцию или 4? Но единого стандарта нет, поэтому иногда между разработчиками возникает непонимание. Кроме того, различные IDE и их компиляторы обрабатывают табуляцию также по-своему. Решением вопроса обычно становится соглашение о правилах форматирования в рамках проекта или языка программирования в целом. Команда разработчиков из Google исследовала проекты в репозитории Github. Они проанализировали код, написанный на 14 языках программирования. Целью исследования было выявить соотношение табуляций и пробелов — то есть, наиболее популярный способ форматирования текста для каждого из языков. Реализация Для анализа использовалась уже существующая таблица [bigquery-public-data:github_repos.sample_files], в которую записаны наименования репозиториев Github. Напомним, что около двух месяцев назад весь открытый код Github стал доступен в форме таблиц BigQuery. Однако для анализа были выбраны не все репозитории, а только верхние 400 тысяч репозиториев с наибольшим числом звёзд, которые они получили за период с января по май 2016 года.",
                    PhotoLink = "https://habrastorage.org/files/b80/b55/a1e/b80b55a1e8d44f9c88a8af81c3caf004.png",
                    PublicationDate = new DateTime(2016, 09, 02)
                }
            );
            context.SaveChanges();
        }

        private static void AddBprograms(FacultyContext context)
        {
            context.UP.AddRange(
                new UndergradProgram
                {
                    Code = "6.040301",
                    Name = "Прикладна математика",
                    Desscription ="Магістерська програма з «Прикладної математики» орієнтована на поглиблене вивчення математичних методів, які широко застосовуються в різних галузях людської діяльності, зокрема, природничих , економічних комп’ютерних науках, соціології. Дана програма орієнтованана підготовку фахівців, здатних до якісного аналізу, побудови  різноманітних складних математичних моделей та проведення точних розрахунків; фахівців, здатних до застосування математичного апарату в природничих, інженерних та комп’ютерних науках, економіці й фінансах, соціології, страховій справі.Програмою даної спеціальності передбачається ґрунтовне вивчення сучасних комп’ютерних технологій, які дають можливість ефективно реалізовувати ",
                    ProfessorId = 1
                },
                new UndergradProgram
                {
                    Code = "6.050103",
                    Name = "Програмна інженерія",
                    Desscription = "Фахівці даного напрямку призначені для роботи на підприємствах усіх форм власності, різного профілю та рівня, в проектних організаціях, консультативних центрах, наукових та освітніх закладах.Загальний обсяг навчальної програми – 240 кредитів ЄКТС, в т.ч.: нормативні дисципліни: гуманітарної та соціальної економічної підготовки – 18 кредитів, фундаментальної природничо-наукової підготовки – 62 кредита, професійної та практичної підготовки – 94 кредити; практичної підготовки – 4 кредита; державна атестація – 7,5 кредита; вибіркові дисципліни: гуманітарної та соціальної економічної підготовки – 6 кредитів, фундаментальної природничо-наукової підготовки – 6,5 кредита, професійної та практичної підготовки – 21 кредит, дисципліни вільного вибору студентів – 21 кредит.",
                    ProfessorId = 3
                }
            );
            context.SaveChanges();
        }

        private static void AddMprograms(FacultyContext context)
        {
            context.MP.AddRange(
                new MasterProgram
                {
                    Code = "8.04030302",
                    Name = "Системи і методи прийняття рішень",
                    Desscription ="Магістерська програма з «Прикладної математики» орієнтована на поглиблене вивчення математичних методів, які широко застосовуються в різних галузях людської діяльності, зокрема, природничих , економічних комп’ютерних науках, соціології. Дана програма орієнтована на підготовку фахівців, здатних до якісного аналізу, побудови  різноманітних складних математичних моделей та проведення точних розрахунків; фахівців, здатних до застосування математичного апарату в природничих, інженерних та комп’ютерних науках, економіці й фінансах, соціології, страховій справі.Програмою даної спеціальності передбачається ґрунтовне вивчення сучасних комп’ютерних технологій, які дають",
                    ProfessorId = 1
                },
                new MasterProgram
                {
                    Code = "8.05010101",
                    Name = "Інформаційні управляючі системи та технології",
                    Desscription = "Фахівці даного напрямку призначені для роботи на підприємствах усіх форм власності, різного профілю та рівня, в проектних організаціях, консультативних центрах, наукових та освітніх закладах.Загальний обсяг навчальної програми – 240 кредитів ЄКТС, в т.ч.: нормативні дисципліни: гуманітарної та соціальної економічної підготовки – 18 кредитів, фундаментальної природничо-наукової підготовки – 62 кредита, професійної та практичної підготовки – 94 кредити; практичної підготовки – 4 кредита; державна атестація – 7,5 кредита; вибіркові дисципліни: гуманітарної та соціальної економічної підготовки – 6 кредитів, фундаментальної природничо-наукової підготовки – 6,5 кредита, професійної та практичної підготовки – 21 кредит, дисципліни вільного вибору студентів – 21 кредит.",
                    ProfessorId = 2
                }
            );
            context.SaveChanges();
        }

        private static void AddReviews(FacultyContext context)
        {
            context.Reviews.AddRange(
                new Review
                {
                    Author = "Мелешевич Андрій Анатолійович",
                    PhotoLink = "http://www.ukma.edu.ua/images/pics/president/melesh-president4.jpg",
                    Title = "Mauris finibus pharetra porttitor",
                    Content = "Sed a sollicitudin felis. Proin suscipit iaculis rhoncus. Vivamus sagittis ligula sit amet purus bibendum feugiat. Pellentesque quis eros mi. Phasellus dapibus ligula venenatis massa eleifend, sed mattis neque pharetra."
                },
                new Review
                {
                    Author = "Брюховецький В'ячеслав Степанович",
                    PhotoLink = "http://www.ukma.edu.ua/images/pics/contacs/prezydent.jpg",
                    Title = "Sed eget pretium ipsum",
                    Content = "Quisque et imperdiet eros, quis imperdiet libero. Donec at massa mattis, molestie risus sit amet, sodales ipsum. Aenean consectetur quam quis urna lacinia suscipit. Morbi quis nibh nibh."
                }
            );
            context.SaveChanges();
        }

        private static void AddCompanies(FacultyContext context)
        {
            context.Companies.AddRange(
                new Company
                {
                    WebsiteLink = "https://google.com",
                    LogoLink = "https://i.kinja-img.com/gawker-media/image/upload/s--pEKSmwzm--/c_scale,fl_progressive,q_80,w_800/1414228815325188681.jpg"
                },
                new Company
                {
                    WebsiteLink = "https://facebook.com",
                    LogoLink = "https://www.facebookbrand.com/img/fb-art.jpg"
                },
                new Company
                {
                    WebsiteLink = "https://twitter.com",
                    LogoLink = "https://www.seeklogo.net/wp-content/uploads/2014/12/twitter-logo-vector-download.jpg"
                }
            );
            context.SaveChanges();
        }

        private static void AddContacts(FacultyContext context)
        {
            context.Contacts.AddRange(
                new Contact
                {
                    Country = "Україна",
                    City = "Київ",
                    ZipCode = "04655",
                    Street = "Сковороди",
                    Building = 2,
                    Office = 315,
                    Telephone1 = "+38 (044) 425-60-22",
                    Telephone2 = "+38 (044) 425-54-17",
                    Fax = "8 (800) 555-35-35",
                    Email = "ukma@ukma.kiev.ua",
                    Website = "fin.ukma.edu.ua",
                    Latitude = 50.46334,
                    Longitude = 30.517199
                }
            );
            context.SaveChanges();
        }

        public static void Initialize(IServiceProvider serviceProvider)
        {
            using (var context = new FacultyContext(
                serviceProvider.GetRequiredService<DbContextOptions<FacultyContext>>()))
            {
                if (!context.Adm.Any())
                    AddAdmin(context);

                if (!context.Professors.Any())
                    AddProfessors(context);

                if (!context.Facts.Any())
                    AddFacts(context);

                if (!context.NewsItems.Any())
                    AddNews(context);

                if (!context.UP.Any())
                    AddBprograms(context);

                if (!context.MP.Any())
                    AddMprograms(context);

                if (!context.Reviews.Any())
                    AddReviews(context);

                if (!context.Companies.Any())
                    AddCompanies(context);

                if (!context.Contacts.Any())
                    AddContacts(context);
            }
        }
    }
}
