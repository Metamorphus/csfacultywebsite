﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FacultyLanding.Models
{
    public class Fact
    {
        [Required]
        public int FactId { get; set; }

        [Required]
        public string Pic { get; set; }

        [Required]
        public string Content { get; set; }
    }
}
