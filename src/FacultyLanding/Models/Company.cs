﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace FacultyLanding.Models
{
    public class Company
    {
        public int CompanyId { get; set; }

        [Required]
        [Url]
        [MaxLength(100)]
        public string WebsiteLink { get; set; }

        [Required]
        public string LogoLink { get; set; }
    }
}
