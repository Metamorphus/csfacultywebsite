﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FacultyLanding.Models
{
    public class Contact
    {
        [Required]
        public int ContactId { get; set; }

        [Required, MaxLength(50)]
        public string Country { get; set; }

        [Required, MaxLength(50)]
        public string City { get; set; }

        [Required]
        public string ZipCode { get; set; }

        [Required, MaxLength(50)]
        public string Street { get; set; }

        [Required]
        public int Building { get; set; }

        [Required]
        public int Office { get; set; }

        [Required,Phone]
        public string Telephone1 { get; set; }

        [Required,Phone]
        public string Telephone2 { get; set; }

        [Required]
        public string Fax { get; set; }

        [Required,EmailAddress]
        public string Email { get; set; }

        [Required, MaxLength(50)]
        public string Website { get; set; }

        [Required]
        public double Longitude { get; set; }

        [Required]
        public double Latitude { get; set; }

    }
}
