using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace FacultyLanding.Middleware
{
    public class ProtectPathOptions
    {
        public string Path { get; set; }
        public string PolicyName { get; set; }
    }

    public static class ProtectPathExtensions
    {
        public static IApplicationBuilder UseProtectPath(
            this IApplicationBuilder builder,
            ProtectPathOptions options)
        {
            return builder.UseMiddleware<ProtectPath>(options);
        }
    }

    public class ProtectPath
    {
        private readonly RequestDelegate _next;
        private readonly string _path;
        private readonly string _policyName;

        public ProtectPath(RequestDelegate next, ProtectPathOptions options)
        {
            _next = next;
            _path = options.Path;
            _policyName = options.PolicyName;
        }

        public async Task Invoke(HttpContext httpContext,
                                 IAuthorizationService authorizationService)
        {
            if(httpContext.Request.Path.Value.Contains(_path))
            {
                var authorized = await authorizationService.AuthorizeAsync(
                                    httpContext.User, null, _policyName);
                if (!authorized)
                {
                    await httpContext.Authentication.ChallengeAsync();
                    return;
                }
            }

            await _next(httpContext);
        }
    }
}
